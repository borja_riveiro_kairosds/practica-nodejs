import 'reflect-metadata';

import { config } from 'dotenv';
config();

import { json } from 'body-parser';
import cors from 'cors';
import express from 'express';
import passport from 'passport';
import swaggerUI from 'swagger-ui-express';
import Container from 'typedi';
import YAML from 'yamljs';
import { PostCacheRepositoryMongo } from './infrastructure/cache/post-cache.repository.mongo';
import { errorMiddleware } from './infrastructure/middlewares/errors';
import passportMiddleware from './infrastructure/middlewares/passport';
import { OffensiveWordRepositoryMongo } from './infrastructure/repositories/offensive-word.repository.mongo';
import { PostRepositoryPostgreSQL } from './infrastructure/repositories/post.repository.postgresql';
import { UserRepositoryPostgreSQL } from './infrastructure/repositories/user.repository.postgresql';
import { offensiveWordsRouter } from './infrastructure/routes/offensive-words.routes';
import { postRouter } from './infrastructure/routes/post.routes';
import { statusRouter } from './infrastructure/routes/status.routes';
import { userRouter } from './infrastructure/routes/user.routes';

Container.set('OffensiveWordRepository', new OffensiveWordRepositoryMongo());
Container.set('UserRepository', new UserRepositoryPostgreSQL());
Container.set('PostRepository', new PostRepositoryPostgreSQL());
Container.set('PostCacheRepository', new PostCacheRepositoryMongo());

console.log('App started');

const app = express();
// Config
app.use(json());
app.use(cors());

// Routes
app.use('/api', offensiveWordsRouter);
app.use('/api', userRouter);
app.use('/api', statusRouter);
app.use('/api', postRouter);

//Docs
const swaggerDocument = YAML.load('./open-api.yaml');
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocument));

// Authorization & Authentication
app.use(passport.initialize());
passport.use(passportMiddleware);

// Errors
app.use(errorMiddleware);

export default app;

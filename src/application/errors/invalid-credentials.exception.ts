import { AplicationUnauthorizedException } from './application-unauthorized.exception';

export class InvalidCredentialsException extends AplicationUnauthorizedException {
  public constructor() {
    super('Wrong credentials');
  }
}

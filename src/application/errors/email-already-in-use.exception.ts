import { AplicationConflictException } from './application-conflict.exception';

export class EmailAlreadyInUseException extends AplicationConflictException {
  public constructor() {
    super('Email already in use');
  }
}

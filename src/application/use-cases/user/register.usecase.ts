import { Service } from 'typedi';
import { User } from '../../../domain/model/entities/user.entity';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { EmailVO } from '../../../domain/model/vos/user/email.vo';
import { PasswordVO } from '../../../domain/model/vos/user/password.vo';
import { Role, RoleVO } from '../../../domain/model/vos/user/role.vo';
import { UserService } from '../../../domain/services/user.service';
import { EmailAlreadyInUseException } from '../../errors/email-already-in-use.exception';
import { UserRequest } from './user.request';

@Service()
export class RegisterUsecase {

  constructor(private userService: UserService) {}

  public async execute (registerRequest: UserRequest): Promise<void> {
    const id = IdVO.create();
    const email = EmailVO.create(registerRequest.email);
    const password = PasswordVO.create(registerRequest.password);
    const role = RoleVO.create(Role.USER);

    const existingUserByEmail = await this.userService.findByEmail(email);

    if (existingUserByEmail) {
      throw new EmailAlreadyInUseException();
    }
    await this.userService.create(new User({id, email, password, role}));
  }
}

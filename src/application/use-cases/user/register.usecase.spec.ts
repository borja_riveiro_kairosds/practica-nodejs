
jest.mock(
  '../../../infrastructure/repositories/user.repository.postgresql.ts',
  () => {
    return {
      UserRepositoryPostgreSQL: jest.fn().mockImplementation(() => {
        return {
          create: jest.fn(),
          findByEmail: jest.fn()
        };
      }),
    };
  }
);



import 'reflect-metadata';
import Container from 'typedi';
import { UserRepositoryPostgreSQL } from '../../../infrastructure/repositories/user.repository.postgresql';
import { RegisterUsecase } from './register.usecase';
import { UserRequest } from './user.request';
describe('Register usecase', () => {
  test('Should register', async () => {
    const repository = new UserRepositoryPostgreSQL();
    Container.set('UserRepository', repository);

    const useCase = Container.get(RegisterUsecase);

    const userRequest: UserRequest = {
      email: 'borja.riveiro@hotmail.com',
      password: 'borja.riveiro'
    };

    await useCase.execute(userRequest);

    expect(repository.create).toHaveBeenCalled();
  });
});

import { sign } from 'jsonwebtoken';
import { Service } from 'typedi';
import { EmailVO } from '../../../domain/model/vos/user/email.vo';
import { PasswordVO } from '../../../domain/model/vos/user/password.vo';
import { UserService } from '../../../domain/services/user.service';
import { InvalidCredentialsException } from '../../errors/invalid-credentials.exception';
import { UserRequest } from './user.request';

@Service()
export class LoginUsecase {

  constructor(private userService: UserService) {}

  public async execute (loginRequest: UserRequest): Promise<string | null> {
    const email = EmailVO.create(loginRequest.email);
    const user = await this.userService.findByEmail(email);
    if (!user) {
      throw new InvalidCredentialsException();
    }
    const password = PasswordVO.create(loginRequest.password);
    const isValidPassword = await this.userService.isValidPassword(password,  user);

    if (!isValidPassword) {
      throw new InvalidCredentialsException();
    }
    return sign({email: user.email.value}, 'gmtPzWoi9%9FKPMt%2eQ', {expiresIn: 86400});
  }
}

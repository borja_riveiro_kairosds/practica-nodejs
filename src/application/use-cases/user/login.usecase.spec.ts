jest.mock('../../../infrastructure/repositories/user.repository.postgresql.ts', () => {
  return {
    UserRepositoryPostgreSQL: jest.fn().mockImplementation(() => {
      return {
        findByEmail: jest.fn().mockImplementation(() =>
          new User({
            id: IdVO.createWithUUID('acbd1c96-ce94-4687-8abe-5c69d06f4ab6'),
            email: EmailVO.create('borja.riveiro@kairosds.com'),
            password: PasswordVO.create('$2b$10$tcFXqybbrkVvPBd4eOggpeeZ62P2f6m6ybTMsh3E6nWUAXcA7oGpW'),
            role: RoleVO.create(Role.USER),
          })
        )
      };
    })
  };
});

import 'reflect-metadata';
import Container from 'typedi';
import { User } from '../../../domain/model/entities/user.entity';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { EmailVO } from '../../../domain/model/vos/user/email.vo';
import { PasswordVO } from '../../../domain/model/vos/user/password.vo';
import { Role, RoleVO } from '../../../domain/model/vos/user/role.vo';
import { UserRepositoryPostgreSQL } from '../../../infrastructure/repositories/user.repository.postgresql';
import { LoginUsecase } from './login.usecase';
import { UserRequest } from './user.request';

describe('Login use case', () => {

  it('Should login', async () => {

    const repository = new UserRepositoryPostgreSQL();
    Container.set('UserRepository', repository);

    const useCase = Container.get(LoginUsecase);
    const loginRequest: UserRequest = {
      email: 'borja.riveiro@kairosds.com',
      password: 'borja.riveiro'
    };

    const user = await useCase.execute(loginRequest);

    expect(repository.findByEmail).toHaveBeenCalled();
    expect(user).toBeTruthy();
  });

});

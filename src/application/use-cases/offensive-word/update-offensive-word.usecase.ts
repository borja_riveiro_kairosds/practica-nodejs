import { Service } from 'typedi';
import { OffensiveWordType } from '../../../domain/model/entities/offensive-word.entity';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { LevelVO } from '../../../domain/model/vos/offensive-words/level.vo';
import { WordVO } from '../../../domain/model/vos/offensive-words/word.vo';
import { OffensiveWordService } from '../../../domain/services/offensive-word.service';
import { OffensiveWordRequest } from './offensive-word.request';
import { OffensiveWordResponse } from './offensive-word.response';

@Service()
export class UpdateOffensiveWordUsecase {

  constructor (private offensiveWordService: OffensiveWordService) {}

  public async execute(id: string, offensiveWordRequest: OffensiveWordRequest): Promise<OffensiveWordResponse | null> {

    const offendiveWordData: OffensiveWordType = {
      id: IdVO.createWithUUID(id),
      word: WordVO.create(offensiveWordRequest.word),
      level: LevelVO.create(offensiveWordRequest.level)
    };

    const offensiveWordUpdated =  await this.offensiveWordService.update(offendiveWordData);

    if (!offensiveWordUpdated) {
      return null;
    }

    const offensiveWordResponse: OffensiveWordResponse = {
      id: offensiveWordUpdated?.id,
      word: offensiveWordUpdated?.word,
      level: offensiveWordUpdated?.level
    };

    return offensiveWordResponse;
  }
}

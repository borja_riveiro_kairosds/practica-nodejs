jest.mock('../../../infrastructure/repositories/offensive-word.repository.mongo.ts');

import 'reflect-metadata';
import Container from 'typedi';
import { OffensiveWordRepositoryMongo } from '../../../infrastructure/repositories/offensive-word.repository.mongo';
import { CreateOffensiveWordUseCase } from './create-offensive-word.usecase';
import { OffensiveWordRequest } from './offensive-word.request';

describe('Create offensive word use case', () => {
  it('Should create offensive word and persist', () => {

    const repository = new OffensiveWordRepositoryMongo();
    Container.set('OffensiveWordRepository', repository);

    const useCase = Container.get(CreateOffensiveWordUseCase);
    const offensiveWordRequest: OffensiveWordRequest = {
      word: 'Test',
      level: 3
    };
    useCase.execute(offensiveWordRequest);

    expect(repository.save).toHaveBeenCalled();
  });
});

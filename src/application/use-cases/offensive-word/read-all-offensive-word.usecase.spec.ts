
jest.mock(
  '../../../infrastructure/repositories/offensive-word.repository.mongo',
  () => {
    return {
      OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
        return {
          findAll: jest.fn().mockImplementation(() => {
            return [
              new OffensiveWord({
                id: IdVO.createWithUUID('acbd1c96-ce94-4687-8abe-5c69d06f4ab6'),
                word: WordVO.create('caca'),
                level: LevelVO.create(2),
              }),
            ];
          }),
        };
      }),
    };
  }
);

import 'reflect-metadata';
import Container from 'typedi';
import { OffensiveWord } from '../../../domain/model/entities/offensive-word.entity';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { LevelVO } from '../../../domain/model/vos/offensive-words/level.vo';
import { WordVO } from '../../../domain/model/vos/offensive-words/word.vo';
import { OffensiveWordRepositoryMongo } from '../../../infrastructure/repositories/offensive-word.repository.mongo';
import { ReadAllOffensiveWordUsecase } from './read-all-offensive-word.usecase';
describe('Find all offensive words use case', () => {
  test('Should find all offensive words', async () => {
    const repository = new OffensiveWordRepositoryMongo();
    Container.set('OffensiveWordRepository', repository);

    const useCase = Container.get(ReadAllOffensiveWordUsecase);

    const offensiveWords = await useCase.execute();

    expect(repository.findAll).toHaveBeenCalled();
    expect(offensiveWords[0].id).toEqual('acbd1c96-ce94-4687-8abe-5c69d06f4ab6');
    expect(offensiveWords[0].word).toEqual('caca');
    expect(offensiveWords[0].level).toEqual(2);
  });
});

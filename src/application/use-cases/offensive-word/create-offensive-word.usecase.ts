import { Service } from 'typedi';
import { OffensiveWordType } from '../../../domain/model/entities/offensive-word.entity';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { LevelVO } from '../../../domain/model/vos/offensive-words/level.vo';
import { WordVO } from '../../../domain/model/vos/offensive-words/word.vo';
import { OffensiveWordService } from '../../../domain/services/offensive-word.service';
import { OffensiveWordRequest } from './offensive-word.request';

@Service()
export class CreateOffensiveWordUseCase {

  constructor(private offensiveWordService: OffensiveWordService) {}

  /*
    El caso de uso recibe una request con el formato en el que el cliente nos va enviar los datos,
    tranforma esos datos a nuestro modelo y llama al servicio del dominio que hemos inyectado por constructor
  */

  public execute (offensiveWordRequest: OffensiveWordRequest) {
    const offensiveWordData: OffensiveWordType = {
      id: IdVO.create(),
      word: WordVO.create(offensiveWordRequest.word),
      level: LevelVO.create(offensiveWordRequest.level)
    };
    this.offensiveWordService.persist(offensiveWordData);
    return {
      id: offensiveWordData.id.value,
      word: offensiveWordData.word.value,
      level: offensiveWordData.level.value
    };
  }
}

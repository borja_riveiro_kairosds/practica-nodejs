import { Service } from 'typedi';
import { OffensiveWordService } from '../../../domain/services/offensive-word.service';
import { OffensiveWordResponse } from './offensive-word.response';

@Service()
export class ReadAllOffensiveWordUsecase {

  constructor (private offensiveWordService: OffensiveWordService) {}

  public async execute(): Promise<OffensiveWordResponse[]> {
    const offensiveWords = await this.offensiveWordService.findAll();
    const offensiveWordsResponse: OffensiveWordResponse[] = offensiveWords.map(offensiveWord => {
      const offensiveWordResponse: OffensiveWordResponse = {
        id: offensiveWord.id,
        word: offensiveWord.word,
        level: offensiveWord.level
      };
      return offensiveWordResponse;
    });
    return offensiveWordsResponse;
  }
}

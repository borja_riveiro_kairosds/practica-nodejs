jest.mock(
  '../../../infrastructure/repositories/offensive-word.repository.mongo',
  () => {
    return {
      OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
        return {
          delete: jest.fn(),
          findById: jest.fn().mockImplementation(() =>
            new OffensiveWord({
              id: IdVO.create(),
              word: WordVO.create('caca'),
              level: LevelVO.create(2)
            })
          )
        };
      }),
    };
  }
);

import 'reflect-metadata';
import Container from 'typedi';
import { OffensiveWord } from '../../../domain/model/entities/offensive-word.entity';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { LevelVO } from '../../../domain/model/vos/offensive-words/level.vo';
import { WordVO } from '../../../domain/model/vos/offensive-words/word.vo';

import { OffensiveWordRepositoryMongo } from '../../../infrastructure/repositories/offensive-word.repository.mongo';
import { DeleteOffensiveWordUsecase } from './delete-offensive-word.usecase';

describe('Delete offensive words use case', () => {
  test('Should delete offensive word', async () => {
    const offensiveWordRepository = new OffensiveWordRepositoryMongo();
    Container.set('OffensiveWordRepository', offensiveWordRepository);

    const useCase = Container.get(DeleteOffensiveWordUsecase);

    await useCase.execute('acbd1c96-ce94-4687-8abe-5c69d06f4ab6');
    expect(offensiveWordRepository.delete).toHaveBeenCalled();
  });
});

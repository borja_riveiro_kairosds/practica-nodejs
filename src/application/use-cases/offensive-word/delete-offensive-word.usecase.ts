import { Service } from 'typedi';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { OffensiveWordService } from '../../../domain/services/offensive-word.service';
import { OffensiveWordResponse } from './offensive-word.response';

@Service()
export class DeleteOffensiveWordUsecase {

  constructor (private offensiveWordService: OffensiveWordService) {}

  public async execute(id: string): Promise<OffensiveWordResponse | null> {

    const idVO = IdVO.createWithUUID(id);

    const offensiveWordDeleted = await this.offensiveWordService.delete(idVO);
    if (!offensiveWordDeleted) {
      return null;
    }

    const offensiveWordResponse: OffensiveWordResponse = {
      id: offensiveWordDeleted?.id,
      word: offensiveWordDeleted?.word,
      level: offensiveWordDeleted?.level
    };

    return offensiveWordResponse;
  }
}

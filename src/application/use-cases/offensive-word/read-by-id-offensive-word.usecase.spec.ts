
jest.mock(
  '../../../infrastructure/repositories/offensive-word.repository.mongo',
  () => {
    return {
      OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
        return {
          findById: jest.fn().mockImplementation(() =>
            new OffensiveWord({
              id: IdVO.createWithUUID('acbd1c96-ce94-4687-8abe-5c69d06f4ab6'),
              word: WordVO.create('caca'),
              level: LevelVO.create(2),
            })
          )
        };
      }),
    };
  }
);

import 'reflect-metadata';
import Container from 'typedi';
import { OffensiveWord } from '../../../domain/model/entities/offensive-word.entity';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { LevelVO } from '../../../domain/model/vos/offensive-words/level.vo';
import { WordVO } from '../../../domain/model/vos/offensive-words/word.vo';
import { OffensiveWordRepositoryMongo } from '../../../infrastructure/repositories/offensive-word.repository.mongo';
import { ReadByIdOffensiveWordUsecase } from './read-by-id-offensive-word.usecase';
describe('Find by id offensive words use case', () => {
  test('Should find by id offensive words', async () => {
    const repository = new OffensiveWordRepositoryMongo();
    Container.set('OffensiveWordRepository', repository);

    const useCase = Container.get(ReadByIdOffensiveWordUsecase);

    const offensiveWord = await useCase.execute('acbd1c96-ce94-4687-8abe-5c69d06f4ab6');

    if (offensiveWord) {
      expect(repository.findById).toHaveBeenCalled();
      expect(offensiveWord.id).toEqual('acbd1c96-ce94-4687-8abe-5c69d06f4ab6');
      expect(offensiveWord.word).toEqual('caca');
      expect(offensiveWord.level).toEqual(2);
    }
  });
});

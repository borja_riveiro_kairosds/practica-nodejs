import { Service } from 'typedi';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { OffensiveWordService } from '../../../domain/services/offensive-word.service';
import { OffensiveWordResponse } from './offensive-word.response';

@Service()
export class ReadByIdOffensiveWordUsecase {
  constructor(private offensiveWordService: OffensiveWordService) {}

  public async execute(id: string): Promise<OffensiveWordResponse | null> {
    const idVO = IdVO.createWithUUID(id);

    const offensiveWord = await this.offensiveWordService.findById(idVO);

    if (!offensiveWord) {
      return null;
    }

    return {
      id: offensiveWord.id,
      word: offensiveWord.word,
      level: offensiveWord.level
    };
  }
}

import 'reflect-metadata';
import Container from 'typedi';
import { User } from '../../../domain/model/entities/user.entity';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { EmailVO } from '../../../domain/model/vos/user/email.vo';
import { PasswordVO } from '../../../domain/model/vos/user/password.vo';
import { Role, RoleVO } from '../../../domain/model/vos/user/role.vo';
import { PostRepositoryPostgreSQL } from '../../../infrastructure/repositories/post.repository.postgresql';
import { CreatePostUsecase } from './create-post.usecase';
import { PostRequest } from './post.request';

jest.mock('../../../infrastructure/repositories/post.repository.postgresql.ts', () => {
  return {
    PostRepositoryPostgreSQL: jest.fn().mockImplementation(() => {
      return {
        create: jest.fn().mockImplementation()
      };
    })
  };
});

describe('Create post usecase', () => {
  it('Should create post and persist', (async () => {

    const repository = new PostRepositoryPostgreSQL();
    Container.set('PostRepository', repository);

    const useCase = Container.get(CreatePostUsecase);
    const postData: PostRequest = {
      author: new User({
        id: IdVO.create(),
        email: EmailVO.create('user@user.com'),
        password:  PasswordVO.create('user'),
        role: RoleVO.create(Role.AUTHOR)
      }),
      title: 'Titulo de post',
      content: 'Contenido de post para prueba unitaria del caso de uso de crear un post.'
    };
    await useCase.execute(postData);

    expect(repository.create).toHaveBeenCalled();
  }));
});

import 'reflect-metadata';
import Container from 'typedi';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { PostRepositoryPostgreSQL } from '../../../infrastructure/repositories/post.repository.postgresql';
import { DeletePostUsecase } from './delete-post.usecase';

jest.mock('../../../infrastructure/repositories/post.repository.postgresql.ts', () => {
  return {
    PostRepositoryPostgreSQL: jest.fn().mockImplementation(() => {
      return {
        delete: jest.fn().mockImplementation()
      };
    })
  };
});


describe('Delete post usecase', () => {
  it('Should delete post', (async () => {

    const repository = new PostRepositoryPostgreSQL();
    Container.set('PostRepository', repository);

    const useCase = Container.get(DeletePostUsecase);
    const postId = IdVO.create().value;
    await useCase.execute(postId);

    expect(repository.delete).toHaveBeenCalled();
  }));
});

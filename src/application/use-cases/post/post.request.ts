import { User } from '../../../domain/model/entities/user.entity';

export type PostRequest = {
  author: User,
  title: string,
  content: string
}

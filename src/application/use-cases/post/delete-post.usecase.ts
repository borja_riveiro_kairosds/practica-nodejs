import { Service } from 'typedi';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { PostService } from '../../../domain/services/post.service';
import { PostResponse } from './post.response';

@Service()
export class DeletePostUsecase {

  constructor(private postService: PostService) {}

  public async execute (postId: string): Promise<PostResponse | null> {
    const post = await this.postService.delete(IdVO.createWithUUID(postId));

    if (!post) {
      return null;
    }

    const postResponse: PostResponse = {
      id: post.id.value,
      author: post.author.email.value,
      title: post.title.value,
      content: post.content.value
    };

    return postResponse;
  }
}

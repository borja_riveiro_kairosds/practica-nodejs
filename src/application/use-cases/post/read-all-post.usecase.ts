import { Service } from 'typedi';
import { PostService } from '../../../domain/services/post.service';
import { PostResponse } from './post.response';

@Service()
export class ReadAllPostUsecase {

  constructor(private postService: PostService) {}

  public async execute (): Promise<PostResponse[]> {
    const findAllResponse = await this.postService.findAll();

    const postsResponse: PostResponse[] = findAllResponse.map(post => {
      const postResponse: PostResponse = {
        id: post.id.value,
        author: post.author.email.value,
        title: post.title.value,
        content: post.content.value
      };
      return postResponse;
    });

    return postsResponse;
  }
}

import 'reflect-metadata';
import Container from 'typedi';
import { Post } from '../../../domain/model/entities/post.entity';
import { User } from '../../../domain/model/entities/user.entity';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { ContentVO } from '../../../domain/model/vos/post/content.vo';
import { TitleVO } from '../../../domain/model/vos/post/title.vo';
import { EmailVO } from '../../../domain/model/vos/user/email.vo';
import { PasswordVO } from '../../../domain/model/vos/user/password.vo';
import { Role, RoleVO } from '../../../domain/model/vos/user/role.vo';
import { PostRepositoryPostgreSQL } from '../../../infrastructure/repositories/post.repository.postgresql';
import { ReadAllPostUsecase } from './read-all-post.usecase';

jest.mock('../../../infrastructure/repositories/post.repository.postgresql.ts', () => {
  return {
    PostRepositoryPostgreSQL: jest.fn().mockImplementation(() => {
      return {
        findAll: jest.fn().mockImplementation(() => {
          return [
            new Post({
              id: IdVO.createWithUUID('acbd1c96-ce94-4687-8abe-5c69d06f4ab6'),
              author: new User({
                id: IdVO.createWithUUID('acbd1c96-ce94-4687-8abe-5c69d06f4ab7'),
                email: EmailVO.create('borja.riveiro@kairosds.com'),
                password: PasswordVO.create('borja.riveiro'),
                role: RoleVO.create(Role.ADMIN),
              }),
              title: TitleVO.create('Post de prueba para test'),
              content: ContentVO.create('Contenido del post de prueba para test unitario con jest'),
              comments: []
            })
          ];
        })
      };
    })
  };
});


describe('Read all post usecase', () => {
  it('Should read posts', (async () => {

    const repository = new PostRepositoryPostgreSQL();
    Container.set('PostRepository', repository);

    const useCase = Container.get(ReadAllPostUsecase);
    const posts = await useCase.execute();

    expect(repository.findAll).toHaveBeenCalled();
    expect(posts[0].id).toBe('acbd1c96-ce94-4687-8abe-5c69d06f4ab6');
    expect(posts[0].author).toBe('borja.riveiro@kairosds.com');
    expect(posts[0].title).toBe('Post de prueba para test');
    expect(posts[0].content).toBe('Contenido del post de prueba para test unitario con jest');

  }));
});

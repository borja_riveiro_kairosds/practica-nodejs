import { Service } from 'typedi';
import { Post, PostType } from '../../../domain/model/entities/post.entity';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { ContentVO } from '../../../domain/model/vos/post/content.vo';
import { TitleVO } from '../../../domain/model/vos/post/title.vo';
import { PostService } from '../../../domain/services/post.service';
import { PostRequest } from './post.request';
import { PostResponse } from './post.response';

@Service()
export class CreatePostUsecase {

  constructor(private postService: PostService) {}

  public async execute (postRequest: PostRequest): Promise<PostResponse> {

    const postData: PostType = {
      id: IdVO.create(),
      author: postRequest.author,
      title: TitleVO.create(postRequest.title),
      content: ContentVO.create(postRequest.content),
      comments: []
    };
    await this.postService.create(new Post(postData));

    const postResponse: PostResponse = {
      id: postData.id.value,
      author: postData.author.email.value,
      title: postData.title.value,
      content: postData.content.value
    };

    return postResponse;
  }
}

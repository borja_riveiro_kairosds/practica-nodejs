import { Service } from 'typedi';
import { Post, PostType } from '../../../domain/model/entities/post.entity';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { ContentVO } from '../../../domain/model/vos/post/content.vo';
import { TitleVO } from '../../../domain/model/vos/post/title.vo';
import { PostService } from '../../../domain/services/post.service';
import { PostRequest } from './post.request';
import { PostResponse } from './post.response';

@Service()
export class UpdatePostUsecase {

  constructor(private postService: PostService) {}

  public async execute (id: string, postRequest: PostRequest): Promise<PostResponse | null>  {

    const postType: PostType = {
      id: IdVO.createWithUUID(id),
      title: TitleVO.create(postRequest.title),
      content: ContentVO.create(postRequest.content),
      author: postRequest.author,
      comments: []
    };

    const postUpdated = await this.postService.update(new Post(postType));

    if (!postUpdated) {
      return null;
    }

    const postResponse: PostResponse = {
      id: postUpdated.id.value,
      author: postUpdated.author.email.value,
      title: postUpdated.title.value,
      content: postUpdated.content.value
    };

    return postResponse;
  }
}

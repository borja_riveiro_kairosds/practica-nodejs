/* eslint-disable @typescript-eslint/no-explicit-any */
import { Service } from 'typedi';
import { Comment, CommentType } from '../../../../domain/model/entities/comment.entity';
import { CommentContentVO } from '../../../../domain/model/vos/comment/comment-content.vo';
import { TimestampVO } from '../../../../domain/model/vos/comment/timestamp.vo';
import { IdVO } from '../../../../domain/model/vos/id.vo';
import { PostService } from '../../../../domain/services/post.service';
import { CommentRequest } from './comment.request';
import { CommentResponse } from './comment.response';

@Service()
export class UpdateCommentUsecase {

  constructor(private postService: PostService) {}

  public async execute (postId: string, commentId: string, commentRequest: CommentRequest): Promise<CommentResponse> {

    const commentType: CommentType = {
      id: IdVO.createWithUUID(commentId),
      author: commentRequest.author,
      content: CommentContentVO.create(commentRequest.content),
      timestamp: TimestampVO.create()
    };

    const commentUpdated: any = await this.postService.updateComment(IdVO.createWithUUID(postId), new Comment(commentType));

    const commentResponse: CommentResponse = {
      id: commentUpdated.id.value,
      author: commentUpdated.author.email.value,
      content: commentUpdated.content.value,
      timestamp: commentUpdated.timestamp.value
    };

    return commentResponse;
  }
}

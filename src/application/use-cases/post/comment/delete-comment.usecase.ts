import { Service } from 'typedi';
import { IdVO } from '../../../../domain/model/vos/id.vo';
import { PostService } from '../../../../domain/services/post.service';
import { CommentResponse } from './comment.response';

@Service()
export class DeleteCommentUsecase {

  constructor(private postService: PostService) {}

  public async execute (postId: string, commentId: string): Promise<CommentResponse> {
    const comment: any = await this.postService.deleteComment(IdVO.createWithUUID(postId), IdVO.createWithUUID(commentId));

    const commentResponse: CommentResponse = {
      id: comment.id.value,
      author: comment.author.email.value,
      content: comment.content.value,
      timestamp: comment.timestamp.value
    };

    return commentResponse;
  }
}

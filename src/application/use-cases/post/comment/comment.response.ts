export type CommentResponse = {
  id: string,
  author: string,
  content: string,
  timestamp: string
}

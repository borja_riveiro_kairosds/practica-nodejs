import { User } from '../../../../domain/model/entities/user.entity';

export type CommentRequest = {
  author: User,
  content: string
}

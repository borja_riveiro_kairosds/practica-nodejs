import { Service } from 'typedi';
import { Comment, CommentType } from '../../../../domain/model/entities/comment.entity';
import { CommentContentVO } from '../../../../domain/model/vos/comment/comment-content.vo';
import { TimestampVO } from '../../../../domain/model/vos/comment/timestamp.vo';
import { IdVO } from '../../../../domain/model/vos/id.vo';
import { OffensiveWordService } from '../../../../domain/services/offensive-word.service';
import { PostService } from '../../../../domain/services/post.service';
import { CommentRequest } from './comment.request';
import { CommentResponse } from './comment.response';

@Service()
export class CreateCommentUsecase {

  constructor(private postService: PostService, private offensiveWordService: OffensiveWordService) {}

  public async execute (postId: string, commentRequest: CommentRequest): Promise<CommentResponse> {

    const offensiveWords = await this.offensiveWordService.findAll();

    const commentData: CommentType = {
      id: IdVO.create(),
      author: commentRequest.author,
      content: CommentContentVO.create(commentRequest.content, offensiveWords),
      timestamp: TimestampVO.create()
    };

    await this.postService.createComment(IdVO.createWithUUID(postId), new Comment(commentData));

    const commentResponse: CommentResponse = {
      id: commentData.id.value,
      author: commentData.author.email.value,
      content: commentData.content.value,
      timestamp: commentData.timestamp.value
    };

    return commentResponse;
  }
}

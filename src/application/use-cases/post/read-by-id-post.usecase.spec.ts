import 'reflect-metadata';
import Container from 'typedi';
import { Comment } from '../../../domain/model/entities/comment.entity';
import { Post } from '../../../domain/model/entities/post.entity';
import { User } from '../../../domain/model/entities/user.entity';
import { CommentContentVO } from '../../../domain/model/vos/comment/comment-content.vo';
import { TimestampVO } from '../../../domain/model/vos/comment/timestamp.vo';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { ContentVO } from '../../../domain/model/vos/post/content.vo';
import { TitleVO } from '../../../domain/model/vos/post/title.vo';
import { EmailVO } from '../../../domain/model/vos/user/email.vo';
import { PasswordVO } from '../../../domain/model/vos/user/password.vo';
import { Role, RoleVO } from '../../../domain/model/vos/user/role.vo';
import { PostRepositoryPostgreSQL } from '../../../infrastructure/repositories/post.repository.postgresql';
import { ReadByIdPostUsecase } from './read-by-id-post.usecase';

jest.mock('../../../infrastructure/repositories/post.repository.postgresql.ts', () => {
  return {
    PostRepositoryPostgreSQL: jest.fn().mockImplementation(() => {
      return {
        findById: jest.fn().mockImplementation(() => {
          return new Post({
            id: IdVO.createWithUUID('acbd1c96-ce94-4687-8abe-5c69d06f4ab6'),
            author: new User({
              id: IdVO.createWithUUID('acbd1c96-ce94-4687-8abe-5c69d06f4ab7'),
              email: EmailVO.create('borja.riveiro@kairosds.com'),
              password: PasswordVO.create('borja.riveiro'),
              role: RoleVO.create(Role.ADMIN),
            }),
            title: TitleVO.create('Post de prueba para test'),
            content: ContentVO.create('Contenido del post de prueba para test unitario con jest'),
            comments: [
              new Comment({
                id: IdVO.createWithUUID('acbd1c96-ce94-4687-8abe-5c69d06f4ab6'),
                content: CommentContentVO.create('Contenido de prueba para el comentario del post'),
                timestamp: TimestampVO.create(),
                author: new User({
                  id: IdVO.createWithUUID('acbd1c96-ce94-4687-8abe-5c69d06f4ab7'),
                  email: EmailVO.create('borja.riveiro@kairosds.com'),
                  password: PasswordVO.create('borja.riveiro'),
                  role: RoleVO.create(Role.ADMIN),
                }),
              }),
            ],
          });
        })
      };
    })
  };
});


describe('Read post by id usecase', () => {
  it('Should read post and comments', (async () => {

    const repository = new PostRepositoryPostgreSQL();
    Container.set('PostRepository', repository);

    const useCase = Container.get(ReadByIdPostUsecase);
    const posts = await useCase.execute('acbd1c96-ce94-4687-8abe-5c69d06f4ab6');

    if (posts) {
      expect(repository.findById).toHaveBeenCalled();
      expect(posts.id).toBe('acbd1c96-ce94-4687-8abe-5c69d06f4ab6');
      expect(posts.author).toBe('borja.riveiro@kairosds.com');
      expect(posts.title).toBe('Post de prueba para test');
      expect(posts.content).toBe('Contenido del post de prueba para test unitario con jest');
      expect(posts.comments[0].id).toBe('acbd1c96-ce94-4687-8abe-5c69d06f4ab6');
      expect(posts.comments[0].content).toBe('Contenido de prueba para el comentario del post');
      expect(posts.comments[0].author).toBe('borja.riveiro@kairosds.com');
    }
  }));
});

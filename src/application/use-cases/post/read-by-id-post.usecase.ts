import { Service } from 'typedi';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { PostService } from '../../../domain/services/post.service';
import { CommentResponse } from './comment/comment.response';
import { PostByIdResponse } from './post.response';

@Service()
export class ReadByIdPostUsecase {

  constructor(private postService: PostService) {}

  public async execute (postId: string): Promise<PostByIdResponse | null> {
    const post = await this.postService.findById(IdVO.createWithUUID(postId));
    if (!post) {
      return null;
    }

    const commentResponse: CommentResponse[] = post.comments.map((comment) => {
      return {
        id: comment.id.value,
        author: comment.author.email.value,
        content: comment.content.value,
        timestamp: comment.timestamp.value
      };
    });

    const postResponse: PostByIdResponse = {
      id: post.id.value,
      author: post.author.email.value,
      title: post.title.value,
      content: post.content.value,
      comments: commentResponse
    };

    return postResponse;
  }
}

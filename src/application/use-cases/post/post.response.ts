import { CommentResponse } from './comment/comment.response';

export type PostResponse = {
  id: string,
  author: string,
  title: string,
  content: string
}

export type PostByIdResponse = {
  id: string,
  author: string,
  title: string,
  content: string
  comments: CommentResponse[]
}

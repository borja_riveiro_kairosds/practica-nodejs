import 'reflect-metadata';
import Container from 'typedi';
import { Post } from '../../../domain/model/entities/post.entity';
import { User } from '../../../domain/model/entities/user.entity';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { ContentVO } from '../../../domain/model/vos/post/content.vo';
import { TitleVO } from '../../../domain/model/vos/post/title.vo';
import { EmailVO } from '../../../domain/model/vos/user/email.vo';
import { PasswordVO } from '../../../domain/model/vos/user/password.vo';
import { Role, RoleVO } from '../../../domain/model/vos/user/role.vo';
import { PostRepositoryPostgreSQL } from '../../../infrastructure/repositories/post.repository.postgresql';
import { PostRequest } from './post.request';
import { UpdatePostUsecase } from './update-post.usecase';

jest.mock('../../../infrastructure/repositories/post.repository.postgresql.ts', () => {
  return {
    PostRepositoryPostgreSQL: jest.fn().mockImplementation(() => {
      return {
        update: jest.fn().mockImplementation(() => {
          new Post({
            id: IdVO.createWithUUID('acbd1c96-ce94-4687-8abe-5c69d06f4ab6'),
            author: new User({
              id: IdVO.createWithUUID('acbd1c96-ce94-4687-8abe-5c69d06f4ab7'),
              email: EmailVO.create('borja.riveiro@kairosds.com'),
              password: PasswordVO.create('borja.riveiro'),
              role: RoleVO.create(Role.ADMIN),
            }),
            title: TitleVO.create('Post de prueba para test'),
            content: ContentVO.create('Contenido del post de prueba para test unitario con jest'),
            comments: []
          });
        })
      };
    })
  };
});


describe('Updatepost usecase', () => {
  it('Should update post', (async () => {

    const repository = new PostRepositoryPostgreSQL();
    Container.set('PostRepository', repository);

    const postRequest: PostRequest = {
      author: new User({
        id: IdVO.createWithUUID('acbd1c96-ce94-4687-8abe-5c69d06f4ab7'),
        email: EmailVO.create('borja.riveiro@kairosds.com'),
        password: PasswordVO.create('borja.riveiro'),
        role: RoleVO.create(Role.ADMIN),
      }),
      title: 'Post actualizado',
      content: 'Contenido del post de prueba para test unitario con jest actualizado'
    };

    const useCase = Container.get(UpdatePostUsecase);
    await useCase.execute('acbd1c96-ce94-4687-8abe-5c69d06f4ab6', postRequest);

    expect(repository.update).toHaveBeenCalled();
  }));
});

/* eslint-disable @typescript-eslint/no-explicit-any */
import { Comment, CommentType } from '../../domain/model/entities/comment.entity';
import { Post, PostType } from '../../domain/model/entities/post.entity';
import { User } from '../../domain/model/entities/user.entity';
import { CommentContentVO } from '../../domain/model/vos/comment/comment-content.vo';
import { TimestampVO } from '../../domain/model/vos/comment/timestamp.vo';
import { IdVO } from '../../domain/model/vos/id.vo';
import { ContentVO } from '../../domain/model/vos/post/content.vo';
import { TitleVO } from '../../domain/model/vos/post/title.vo';
import { EmailVO } from '../../domain/model/vos/user/email.vo';
import { PasswordVO } from '../../domain/model/vos/user/password.vo';
import { RoleVO } from '../../domain/model/vos/user/role.vo';
import { PostRepository } from '../../domain/repositories/post.repository';
import { IdNotFoundException } from '../errors/id-not-found.exception';
import { CommentModel } from '../schemas/comment.schema';
import { PostModel } from '../schemas/post.schema';
import { UserModel } from '../schemas/user.shcema';

export class PostRepositoryPostgreSQL implements PostRepository {

  // Create Post
  public async create(post: Post): Promise<void> {
    const postData = {
      id: post.id.value,
      title: post.title.value,
      content: post.content.value,
      userId: post.author.id.value
    };
    await PostModel.create(postData);

  }

  // Find all Post
  async findAll(): Promise<Post[]> {
    const findAllResult = await PostModel.findAll({include: UserModel});

    return findAllResult.map((post: any) => {
      const postModel: PostType = {
        id: IdVO.createWithUUID(post.id),
        author: new User({
          id: IdVO.createWithUUID(post.user.id),
          email: EmailVO.create(post.user.email),
          password:  PasswordVO.create(post.user.password),
          role: RoleVO.create(post.user.role)
        }),
        title: TitleVO.create(post.title),
        content: ContentVO.create(post.content),
        comments: []
      };
      return new Post(postModel);
    });
  }

  // Find by id Post
  async findById(postId: IdVO): Promise<Post | null> {

    const postFound: any = await PostModel.findOne({
      where: { id: postId.value },
      include: [UserModel, { model: CommentModel, include: [UserModel] }],
    });

    if (!postFound) {
      throw new IdNotFoundException(postId.value);
    }

    const postType: PostType = {
      id: IdVO.createWithUUID(postFound.id),
      author: new User({
        id: IdVO.createWithUUID(postFound.user.id),
        email: EmailVO.create(postFound.user.email),
        password:  PasswordVO.create(postFound.user.password),
        role: RoleVO.create(postFound.user.role)
      }),
      title: TitleVO.create(postFound.title),
      content: ContentVO.create(postFound.content),
      comments: postFound.comments.map((commentInPost: any) => {
        return new Comment({
          id: IdVO.createWithUUID(commentInPost.id),
          content: CommentContentVO.create(commentInPost.content),
          author: new User({
            id: IdVO.createWithUUID(commentInPost.user.id),
            email: EmailVO.create(commentInPost.user.email),
            password: PasswordVO.create(commentInPost.user.password),
            role: RoleVO.create(commentInPost.user.role),
          }),
          timestamp: TimestampVO.createWithTimestamp(commentInPost.timestamp)
        });
      })
    };
    return new Post(postType);
  }

  // Delete Post
  async delete(postId: IdVO): Promise<Post | null> {

    const postToDelete: any = await PostModel.findByPk(postId.value, {include: UserModel});

    if (!postToDelete) {
      throw new IdNotFoundException(postId.value);
    }

    await postToDelete.destroy();

    const postType: PostType = {
      id: IdVO.createWithUUID(postToDelete.id),
      author: new User({
        id: IdVO.createWithUUID(postToDelete.user.id),
        email: EmailVO.create(postToDelete.user.email),
        password:  PasswordVO.create(postToDelete.user.password),
        role: RoleVO.create(postToDelete.user.role)
      }),
      title: TitleVO.create(postToDelete.title),
      content: ContentVO.create(postToDelete.content),
      comments: []
    };

    return new Post(postType);
  }

  // Update Post
  async update(post: Post): Promise<Post | null> {

    const postToUpdate= await PostModel.findByPk(post.id.value, {include: UserModel});

    if (!postToUpdate) {
      throw new IdNotFoundException(post.id.value);
    }

    const postUpdated: any = await postToUpdate.update({
      title: post.title.value,
      content: post.content.value
    });

    const postType: PostType = {
      id: IdVO.createWithUUID(postUpdated.id),
      author: new User({
        id: IdVO.createWithUUID(postUpdated.user.id),
        email: EmailVO.create(postUpdated.user.email),
        password:  PasswordVO.create(postUpdated.user.password),
        role: RoleVO.create(postUpdated.user.role)
      }),
      title: TitleVO.create(postUpdated.title),
      content: ContentVO.create(postUpdated.content),
      comments: []
    };

    return new Post(postType);
  }

  // Create Comment
  async createComment(postId: IdVO, comment: Comment): Promise<void> {
    const post: any = await PostModel.findByPk(postId.value);

    if (!post) {
      throw new IdNotFoundException(postId.value);
    }

    const commentData: any = await CommentModel.create({
      id: comment.id.value,
      content: comment.content.value,
      timestamp: comment.timestamp.value,
      userId: comment.author.id.value
    });
    await post.addComment(commentData);

  }

  // Delete Comment
  async deleteComment(postId: IdVO, commentId: IdVO): Promise<Comment> {
    const post: any = await PostModel.findByPk(postId.value);

    if (!post) {
      throw new IdNotFoundException(postId.value);
    }

    const commentToDelete: any = await CommentModel.findByPk(commentId.value, {include: UserModel});

    if (!commentToDelete) {
      throw new IdNotFoundException(commentId.value);
    }

    await commentToDelete?.destroy();

    const commentType: CommentType = {
      id: IdVO.createWithUUID(commentToDelete.id),
      author: new User({
        id: IdVO.createWithUUID(commentToDelete.user.id),
        email: EmailVO.create(commentToDelete.user.email),
        password:  PasswordVO.create(commentToDelete.user.password),
        role: RoleVO.create(commentToDelete.user.role)
      }),
      content: CommentContentVO.create(commentToDelete.content),
      timestamp: TimestampVO.createWithTimestamp(commentToDelete.timestamp)
    };

    return new Comment(commentType);
  }

  // Update comment
  async updateComment(postId: IdVO, comment: Comment): Promise<Comment> {
    const post: any = await PostModel.findByPk(postId.value);

    if (!post) {
      throw new IdNotFoundException(postId.value);
    }

    const commentToUpdate: any = await CommentModel.findByPk(comment.id.value, {include: UserModel});

    if (!commentToUpdate) {
      throw new IdNotFoundException(comment.id.value);
    }

    const commentUpdated: any = await commentToUpdate.update({
      content: comment.content.value
    });

    const commentType: CommentType = {
      id: IdVO.createWithUUID(commentUpdated.id),
      author: new User({
        id: IdVO.createWithUUID(commentUpdated.user.id),
        email: EmailVO.create(commentUpdated.user.email),
        password:  PasswordVO.create(commentUpdated.user.password),
        role: RoleVO.create(commentUpdated.user.role)
      }),
      content: CommentContentVO.create(commentUpdated.content),
      timestamp: TimestampVO.createWithTimestamp(commentUpdated.timestamp)
    };

    return new Comment(commentType);
  }

  // Find comment by id
  async findCommentById(commentId: IdVO): Promise<Comment> {
    const comment: any = await CommentModel.findByPk(commentId.value, {include: UserModel});

    const commentType: CommentType = {
      id: IdVO.createWithUUID(comment.id),
      author: new User({
        id: IdVO.createWithUUID(comment.user.id),
        email: EmailVO.create(comment.user.email),
        password:  PasswordVO.create(comment.user.password),
        role: RoleVO.create(comment.user.role)
      }),
      content: CommentContentVO.create(comment.content),
      timestamp: TimestampVO.createWithTimestamp(comment.timestamp)
    };

    return new Comment(commentType);
  }

}

import { OffensiveWord, OffensiveWordType } from '../../domain/model/entities/offensive-word.entity';
import { IdVO } from '../../domain/model/vos/id.vo';
import { LevelVO } from '../../domain/model/vos/offensive-words/level.vo';
import { WordVO } from '../../domain/model/vos/offensive-words/word.vo';
import { OffensiveWordRepository } from '../../domain/repositories/offensive-word.repository';
import { OffensiveWordModel } from '../schemas/offensive-word.schema';

export class OffensiveWordRepositoryMongo implements OffensiveWordRepository {

  save(offensiveWord: OffensiveWord): void {
    const newOffensiveWord = {
      id: offensiveWord.id,
      word: offensiveWord.word,
      level: offensiveWord.level
    };
    const offensiveWordModel = new OffensiveWordModel(newOffensiveWord);

    offensiveWordModel.save();
  }

  async findAll(): Promise<OffensiveWord[]> {
    const allOfensiveWords = await OffensiveWordModel.find({}).exec();
    return allOfensiveWords.map(offensiveWord => {
      const offensiveWordModel = {
        id: IdVO.createWithUUID(offensiveWord.id),
        word: WordVO.create(offensiveWord.word),
        level: LevelVO.create(offensiveWord.level)
      };

      return new OffensiveWord(offensiveWordModel);
    });
  }

  async findById(id: IdVO): Promise<OffensiveWord | null> {
    // return await OffensiveWordModel.findOne({id: id.value});
    const findOffensiveWord = await OffensiveWordModel.findOne({id: id.value}).exec();
    if (findOffensiveWord !== null ) {
      const offensiveWord: OffensiveWordType = {
        id: IdVO.createWithUUID(findOffensiveWord.id),
        word: WordVO.create(findOffensiveWord.word),
        level: LevelVO.create(findOffensiveWord.level)
      };
      return new OffensiveWord(offensiveWord);
    }
    return null;

  }

  async delete(id: IdVO): Promise<OffensiveWord | null> {
    const offensiveWordDeleted = await OffensiveWordModel.findOneAndDelete({id: id.value});
    if (!offensiveWordDeleted) {
      return null;
    }
    const offensiveWord: OffensiveWordType = {
      id: IdVO.createWithUUID(offensiveWordDeleted.id),
      word: WordVO.create(offensiveWordDeleted.word),
      level: LevelVO.create(offensiveWordDeleted.level)
    };
    return new OffensiveWord(offensiveWord);
  }

  async update(offensiveWord: OffensiveWord): Promise<OffensiveWord | null> {

    const offensiveWordSchema = {
      word: offensiveWord.word,
      level: offensiveWord.level
    };

    await OffensiveWordModel.findOneAndUpdate(
      {id: offensiveWord.id},
      offensiveWordSchema
    );

    const idVO = IdVO.createWithUUID(offensiveWord.id);

    return await this.findById(idVO);

  }

}

import { User, UserType } from '../../domain/model/entities/user.entity';
import { IdVO } from '../../domain/model/vos/id.vo';
import { EmailVO } from '../../domain/model/vos/user/email.vo';
import { PasswordVO } from '../../domain/model/vos/user/password.vo';
import { RoleVO } from '../../domain/model/vos/user/role.vo';
import { UserRepository } from '../../domain/repositories/user.repository';
import { UserModel } from '../schemas/user.shcema';

export class UserRepositoryPostgreSQL implements UserRepository {

  async create(user: User): Promise<void> {
    const id = user.id.value;
    const email = user.email.value;
    const password = user.password.value;
    const role = user.role.value;

    const userModel = UserModel.build({id, email, password, role});
    await userModel.save();
  }

  async findByEmail(email: EmailVO): Promise<User | null> {
    const foundUser: any = await UserModel.findOne({where: {email: email.value}});

    if (!foundUser) {
      return null;
    }

    const user: UserType = {
      id: IdVO.createWithUUID(foundUser.id),
      email: EmailVO.create(foundUser.email),
      password: PasswordVO.create(foundUser.password),
      role: RoleVO.create(foundUser.role)
    };

    return new User(user);
  }
}

import express from 'express';
import { AplicationConflictException } from '../../application/errors/application-conflict.exception';
import { AplicationUnauthorizedException } from '../../application/errors/application-unauthorized.exception';
import { DomainFormatException } from '../../domain/errors/domain-format.exception';
import { DomainNotFoundException } from '../../domain/errors/domain-not-found.exception';
import { InfrastructureForbiddenException } from '../errors/infrastructure-forbidden.exception';
import { InfrastructureFormatException } from '../errors/infrastructure-format.exception';
import { InfrastructureNotFoundException } from '../errors/infrastructure-not-found.exception';

export const errorMiddleware = (
  error: express.ErrorRequestHandler,
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) => {

  const BAD_REQUEST = 400;
  const UNAUTHORIZED = 401;
  const FORBIDDEN = 403;
  const NOT_FOUND = 404;
  const CONFLICT = 409;
  const INTERNAL_SERVER_ERROR = 500;

  if (error instanceof DomainFormatException || error instanceof InfrastructureFormatException) {
    return res.status(BAD_REQUEST).json({status: BAD_REQUEST, message: error.message});
  }

  if (error instanceof AplicationUnauthorizedException) {
    return res.status(UNAUTHORIZED).json({status: UNAUTHORIZED, message: error.message});
  }

  if (error.name === 'AuthenticationError') {
    return res.status(UNAUTHORIZED).json({status: UNAUTHORIZED, message: 'Unauthorized'});
  }

  if (error instanceof InfrastructureForbiddenException) {
    return res.status(FORBIDDEN).json({status: FORBIDDEN, message: error.message});
  }

  if (error instanceof DomainNotFoundException || error instanceof InfrastructureNotFoundException) {
    return res.status(NOT_FOUND).json({status: NOT_FOUND, message: error.message});
  }

  if (error instanceof AplicationConflictException) {
    return res.status(CONFLICT).json({status: CONFLICT, message: error.message});
  }

  console.log(error);
  return res.status(INTERNAL_SERVER_ERROR).json({status: INTERNAL_SERVER_ERROR, message: 'Internal server error'});
};

import express from 'express';
import Container from 'typedi';
import { Comment } from '../../domain/model/entities/comment.entity';
import { Post } from '../../domain/model/entities/post.entity';
import { User } from '../../domain/model/entities/user.entity';
import { IdVO } from '../../domain/model/vos/id.vo';
import { Role } from '../../domain/model/vos/user/role.vo';
import { PostService } from '../../domain/services/post.service';
import { NotAllowedException } from '../errors/not-allowed.exception';

export enum Permission {
  POST = 'POST',
  COMMENT = 'COMMENT'
}

export const hasPermissions = (permission: Permission) => {
  return async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction): Promise<express.Response | void> => {
    try {
      const user = req.user as User;
      const postService = Container.get(PostService);

      if (permission === Permission.POST) {
        const { postId } = req.params;
        const post: Post | null = await postService.findById(IdVO.createWithUUID(postId));
        if (user.role.value !== Role.ADMIN && post?.author.id.value !== user.id.value) {
          throw new NotAllowedException();
        }
      }

      if (permission === Permission.COMMENT) {
        const { commentId } = req.params;
        const comment: Comment = await postService.findCommentById(IdVO.createWithUUID(commentId));
        if (user.role.value !== Role.ADMIN && comment.author.id.value !== user.id.value) {
          throw new NotAllowedException();
        }
      }

      next();
      return;

    } catch (error) {
      next(error);
    }
  };
};

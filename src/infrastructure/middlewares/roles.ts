import express from 'express';
import { User } from '../../domain/model/entities/user.entity';
import { Role } from '../../domain/model/vos/user/role.vo';
import { NotAllowedException } from '../errors/not-allowed.exception';

export const hasRole = (roles: Role[]) => {
  return async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction): Promise<express.Response | void> => {
    try {
      const user = req.user as User;
      const roleUser = user.role.value;

      const allow = roles.some(role => roleUser === role);

      if (!allow) {
        throw new NotAllowedException();
      }

      next();
      return;

    } catch (error) {
      next(error);
    }
  };
};

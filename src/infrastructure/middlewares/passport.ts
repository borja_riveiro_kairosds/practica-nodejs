import { ExtractJwt, Strategy, StrategyOptions } from 'passport-jwt';
import Container from 'typedi';
import { EmailVO } from '../../domain/model/vos/user/email.vo';
import { UserService } from '../../domain/services/user.service';

const options: StrategyOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: 'gmtPzWoi9%9FKPMt%2eQ'
};

export default new Strategy(options, async (payload, done) => {
  try {
    const {email} = payload;
    const userService = Container.get(UserService);
    const user = await userService.findByEmail(EmailVO.create(email));
    if (!user) {
      return done(null, false, {message: 'User not found'});
    }
    return done (null, user);
  } catch (error) {
    console.log(error);
  }
});

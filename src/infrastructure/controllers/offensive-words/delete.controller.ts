import express from 'express';
import Container from 'typedi';
import { DeleteOffensiveWordUsecase } from '../../../application/use-cases/offensive-word/delete-offensive-word.usecase';

export const deleteController =
async (req: express.Request , res: express.Response, next: express.NextFunction) => {
  try {
    const { id } = req.params;
    const deleteUseCase = Container.get(DeleteOffensiveWordUsecase);
    const deleteResponse = await deleteUseCase.execute(id);
    return res.status(200).json(deleteResponse);
  } catch (error) {
    next(error);
  }
};

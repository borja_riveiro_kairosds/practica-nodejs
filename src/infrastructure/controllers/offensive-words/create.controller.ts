import express from 'express';
import { validationResult } from 'express-validator';
import Container from 'typedi';
import { CreateOffensiveWordUseCase } from '../../../application/use-cases/offensive-word/create-offensive-word.usecase';
import { OffensiveWordRequest } from '../../../application/use-cases/offensive-word/offensive-word.request';
import { MissingFieldsException } from '../../errors/missing-fields.exception';

export const createController =
  (req: express.Request , res: express.Response, next: express.NextFunction) => {
    try {
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        throw new MissingFieldsException();
      }

      const {word, level} = req.body;
      const offensiveWordRequest: OffensiveWordRequest = {
        word,
        level
      };
      const createUseCase = Container.get(CreateOffensiveWordUseCase);
      const offensiveWordResponse = createUseCase.execute(offensiveWordRequest);
      return res.status(201).json(offensiveWordResponse);
    } catch (error) {
      next(error);
    }
  };

import express from 'express';
import { validationResult } from 'express-validator';
import Container from 'typedi';
import { OffensiveWordRequest } from '../../../application/use-cases/offensive-word/offensive-word.request';
import { UpdateOffensiveWordUsecase } from '../../../application/use-cases/offensive-word/update-offensive-word.usecase';
import { MissingFieldsException } from '../../errors/missing-fields.exception';

export const updateController =
  async (req: express.Request , res: express.Response, next: express.NextFunction) => {
    try {
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        throw new MissingFieldsException();
      }

      const { id } = req.params;
      const {word, level } = req.body;
      const offensiveWordRequest: OffensiveWordRequest = {
        word,
        level
      };
      const updateUseCase = Container.get(UpdateOffensiveWordUsecase);
      const updateResponse = await updateUseCase.execute(id, offensiveWordRequest);
      return res.status(200).json(updateResponse);
    } catch (error) {
      next(error);
    }
  };

import express from 'express';
import Container from 'typedi';
import { ReadByIdOffensiveWordUsecase } from '../../../application/use-cases/offensive-word/read-by-id-offensive-word.usecase';

export const readByIdController =
  async (req: express.Request , res: express.Response, next: express.NextFunction) => {
    try {
      const {id} = req.params;
      const readByIdUseCase = Container.get(ReadByIdOffensiveWordUsecase);
      const readByIdResponse = await readByIdUseCase.execute(id);
      return res.status(200).json(readByIdResponse);
    } catch (error) {
      next(error);
    }
  };

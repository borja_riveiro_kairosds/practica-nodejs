import express from 'express';
import Container from 'typedi';
import { ReadAllOffensiveWordUsecase } from '../../../application/use-cases/offensive-word/read-all-offensive-word.usecase';

export const readAllController =
  async (req: express.Request , res: express.Response, next: express.NextFunction) => {
    try {
      const readAllUseCase = Container.get(ReadAllOffensiveWordUsecase);
      const readAllResponse = await readAllUseCase.execute();
      return res.status(200).json(readAllResponse);
    } catch (error) {
      next(error);
    }
  };

import express from 'express';
import { User } from '../../../domain/model/entities/user.entity';

export const getRoleController =
  (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
      const user = req.user as User;
      const role = user?.role.value;
      return res.status(200).json({role});
    } catch (error) {
      next(error);
    }
  };

import express from 'express';
import { validationResult } from 'express-validator';
import Container from 'typedi';
import { RegisterUsecase } from '../../../application/use-cases/user/register.usecase';
import { UserRequest } from '../../../application/use-cases/user/user.request';
import { MissingFieldsException } from '../../errors/missing-fields.exception';

export const registerController = async (req: express.Request , res: express.Response, next: express.NextFunction) => {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      throw new MissingFieldsException();
    }

    const {email, password} = req.body;
    const userRequest: UserRequest = {
      email,
      password
    };
    const registerUsecase = Container.get(RegisterUsecase);
    await registerUsecase.execute(userRequest);
    return res.status(201).json({status: 'User successfully registered'});
  } catch (error) {
    next(error);
  }
};

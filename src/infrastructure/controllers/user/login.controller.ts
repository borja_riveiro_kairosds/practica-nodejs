import express from 'express';
import { validationResult } from 'express-validator';
import Container from 'typedi';
import { LoginUsecase } from '../../../application/use-cases/user/login.usecase';
import { UserRequest } from '../../../application/use-cases/user/user.request';
import { MissingFieldsException } from '../../errors/missing-fields.exception';

export const loginController = async (req: express.Request , res: express.Response, next: express.NextFunction) => {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      throw new MissingFieldsException();
    }

    const {email, password} = req.body;
    const userRequest: UserRequest = {
      email,
      password
    };

    const loginUsecase = Container.get(LoginUsecase);
    const token = await loginUsecase.execute(userRequest);

    if (!token) {
      return res.status(401).json({error: 'Not permited'});
    }
    return res.status(200).json({token});
  } catch (error) {
    next(error);
  }
};

import express from 'express';

export const statusController = async (req: express.Request , res: express.Response, next: express.NextFunction) => {
  try {
    return res.status(200).send();
  } catch (error) {
    next(error);
  }
};

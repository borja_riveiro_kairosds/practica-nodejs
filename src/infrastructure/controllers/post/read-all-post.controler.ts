import express from 'express';
import Container from 'typedi';
import { ReadAllPostUsecase } from '../../../application/use-cases/post/read-all-post.usecase';

export const readAllPostController =
  async (req: express.Request , res: express.Response, next: express.NextFunction) => {
    try {
      const postUsecase = Container.get(ReadAllPostUsecase);
      const readAllPostResponse = await postUsecase.execute();

      return res.status(200).json(readAllPostResponse);
    } catch (error) {
      next(error);
    }
  };

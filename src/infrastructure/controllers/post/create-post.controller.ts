import express from 'express';
import { validationResult } from 'express-validator';
import Container from 'typedi';
import { CreatePostUsecase } from '../../../application/use-cases/post/create-post.usecase';
import { PostRequest } from '../../../application/use-cases/post/post.request';
import { User } from '../../../domain/model/entities/user.entity';
import { MissingFieldsException } from '../../errors/missing-fields.exception';

export const createPostController =
  async (req: express.Request , res: express.Response, next: express.NextFunction) => {
    try {
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        throw new MissingFieldsException();
      }

      const author  = req.user as User;
      const { title, content } = req.body;
      const postRequest: PostRequest = {
        title,
        content,
        author
      };
      const postUsecase = Container.get(CreatePostUsecase);
      const postResult = await postUsecase.execute(postRequest);

      return res.status(201).json(postResult);
    } catch (error) {
      next(error);
    }
  };

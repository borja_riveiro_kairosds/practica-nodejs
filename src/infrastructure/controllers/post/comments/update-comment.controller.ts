import express from 'express';
import { validationResult } from 'express-validator';
import Container from 'typedi';
import { CommentRequest } from '../../../../application/use-cases/post/comment/comment.request';
import { UpdateCommentUsecase } from '../../../../application/use-cases/post/comment/update-comment.usecase';
import { User } from '../../../../domain/model/entities/user.entity';
import { MissingFieldsException } from '../../../errors/missing-fields.exception';

export const updateCommentController =
  async (req: express.Request , res: express.Response, next: express.NextFunction) => {
    try {
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        throw new MissingFieldsException();
      }

      const { postId, commentId } = req.params;
      const { content } = req.body;
      const author  = req.user as User;

      const commentRequest: CommentRequest = {
        author,
        content
      };

      const commentUsecase = Container.get(UpdateCommentUsecase);
      const commentResult = await commentUsecase.execute(postId, commentId, commentRequest);

      return res.status(200).json(commentResult);
    } catch (error) {
      next(error);
    }
  };

import express from 'express';
import Container from 'typedi';
import { DeleteCommentUsecase } from '../../../../application/use-cases/post/comment/delete-comment.usecase';

export const deleteCommentController =
  async (req: express.Request , res: express.Response, next: express.NextFunction) => {
    try {
      const { postId, commentId } = req.params;
      const commentUsecase = Container.get(DeleteCommentUsecase);
      const commentResult = await commentUsecase.execute(postId, commentId);

      return res.status(200).json(commentResult);
    } catch (error) {
      next(error);
    }
  };

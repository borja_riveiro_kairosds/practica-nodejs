import express from 'express';
import { validationResult } from 'express-validator';
import Container from 'typedi';
import { CommentRequest } from '../../../../application/use-cases/post/comment/comment.request';
import { CreateCommentUsecase } from '../../../../application/use-cases/post/comment/create-comment.usecase';
import { User } from '../../../../domain/model/entities/user.entity';
import { MissingFieldsException } from '../../../errors/missing-fields.exception';


export const createCommentController =
  async (req: express.Request , res: express.Response, next: express.NextFunction) => {
    try {
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        throw new MissingFieldsException();
      }

      const author  = req.user as User;
      const { postId } = req.params;
      const { content } = req.body;

      const commentRequest: CommentRequest = {
        author,
        content
      };
      const commentUsecase = Container.get(CreateCommentUsecase);
      const commentResult = await commentUsecase.execute(postId, commentRequest);

      return res.status(201).json(commentResult);
    } catch (error) {
      next(error);
    }
  };

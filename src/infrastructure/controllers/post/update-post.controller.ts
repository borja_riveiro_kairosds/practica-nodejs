import express from 'express';
import { validationResult } from 'express-validator';
import Container from 'typedi';
import { PostRequest } from '../../../application/use-cases/post/post.request';
import { UpdatePostUsecase } from '../../../application/use-cases/post/update-post.usecase';
import { User } from '../../../domain/model/entities/user.entity';
import { MissingFieldsException } from '../../errors/missing-fields.exception';

export const updatePostController =
  async (req: express.Request , res: express.Response, next: express.NextFunction) => {
    try {
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        throw new MissingFieldsException();
      }

      const { postId } = req.params;
      const { title, content } = req.body;
      const author  = req.user as User;

      const postRequest: PostRequest = {
        author,
        title,
        content
      };
      const postUsecase = Container.get(UpdatePostUsecase);
      const postUpdated = await postUsecase.execute(postId, postRequest);

      return res.status(200).json(postUpdated);
    } catch (error) {
      next(error);
    }
  };

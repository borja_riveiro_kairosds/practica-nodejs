import express from 'express';
import Container from 'typedi';
import { ReadByIdPostUsecase } from '../../../application/use-cases/post/read-by-id-post.usecase';

export const readByIdPostController =
  async (req: express.Request , res: express.Response, next: express.NextFunction) => {
    try {
      const { postId } = req.params;

      const postUsecase = Container.get(ReadByIdPostUsecase);
      const readByIdPostResponse = await postUsecase.execute(postId);

      return res.status(200).json(readByIdPostResponse);
    } catch (error) {
      next(error);
    }
  };

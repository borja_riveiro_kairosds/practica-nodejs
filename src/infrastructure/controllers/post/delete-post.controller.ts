import express from 'express';
import Container from 'typedi';
import { DeletePostUsecase } from '../../../application/use-cases/post/delete-post.usecase';

export const deletePostController =
  async (req: express.Request , res: express.Response, next: express.NextFunction) => {
    try {
      const { postId } = req.params;
      const postUsecase = Container.get(DeletePostUsecase);
      const postDeleted = await postUsecase.execute(postId);

      return res.status(200).json(postDeleted);
    } catch (error) {
      next(error);
    }
  };

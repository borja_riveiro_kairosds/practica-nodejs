import { DataTypes, InferAttributes, InferCreationAttributes, Model } from 'sequelize';
import sequelize from '../config/postgresql';

interface PostModel extends Model<InferAttributes<PostModel>, InferCreationAttributes<PostModel>> {
  // Some fields are optional when calling UserModel.create() or UserModel.build()
  id: string;
  title: string;
  content: string;
}

const PostModel = sequelize.define<PostModel>('posts', {
  id: {
    type: DataTypes.UUID,
    allowNull: false,
    primaryKey: true
  },
  title: {
    type: DataTypes.STRING,
    allowNull: false
  },
  content: {
    type: DataTypes.STRING,
    allowNull: false
  },
});

export { PostModel };

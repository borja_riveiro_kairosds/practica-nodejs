import { DataTypes } from 'sequelize';
import sequelize from '../config/postgresql';

const UserModel = sequelize.define('users', {
  id: {
    type: DataTypes.UUID,
    allowNull: false,
    primaryKey: true
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true
  },
  password: {
    type: DataTypes.STRING,
    allowNull: false
  },
  role: {
    type: DataTypes.ENUM('ADMIN', 'USER', 'AUTHOR'),
    allowNull: false
  }
});

export { UserModel };

import mongoose from 'mongoose';


export const connectToDataBase = async () => {
  try {

    const authSource = process.env.MONGO_AUTHSOURCE ?? 'admin';
    const username = process.env.MONGO_USERNAME ?? 'admin';
    const password = process.env.MONGO_PASSWORD ?? 'admin';

    const host = process.env.MONGO_HOST ?? 'localhost';
    const port = process.env.MONGO_PORT ?? '27017';
    const db_name = process.env.MONGODB_DB_NAME ?? 'blog';
    const url = `mongodb://${host}:${port}/${db_name}`;

    await mongoose.connect(url, {
      authSource: authSource,
      auth: {
        username: username,
        password: password,
      },

    });
  } catch (error) {
    console.log(error);
  }
};

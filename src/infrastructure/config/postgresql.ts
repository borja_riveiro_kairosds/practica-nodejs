import { Sequelize } from 'sequelize';
import { populateAdmin } from './populate';

const user = process.env.POSTGRESQL_USER ?? 'pguser';
const pass = process.env.POSTGRESQL_PASS ?? 'pguser';
const host = process.env.POSTGRESQL_HOST ?? 'localhost';
const port = process.env.POSTGRESQL_PORT ?? '5432';
const dbName = process.env.POSTGRESQL_DBNAME ?? 'pgdb';

const sequelize = new Sequelize(`postgres://${user}:${pass}@${host}:${port}/${dbName}`);

sequelize.authenticate()
  .then(() => console.log('Connection postgresql established'))
  .catch((err) => console.log(err));

sequelize.sync({ force: true })
  .then(() => {
    console.log('Database & tables created');
  }).then(() => {
    populateAdmin();
  });

export default sequelize;

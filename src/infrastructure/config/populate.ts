import Container from 'typedi';
import { User, UserType } from '../../domain/model/entities/user.entity';
import { IdVO } from '../../domain/model/vos/id.vo';
import { EmailVO } from '../../domain/model/vos/user/email.vo';
import { PasswordVO } from '../../domain/model/vos/user/password.vo';
import { Role, RoleVO } from '../../domain/model/vos/user/role.vo';
import { UserService } from '../../domain/services/user.service';

const populateAdmin = async(): Promise<void> => {
  const userService = Container.get(UserService);
  const adminUser: UserType = {
    id: IdVO.create(),
    email: EmailVO.create('borja.riveiro@kairosds.com'),
    password: PasswordVO.create('borja.riveiro'),
    role: RoleVO.create(Role.ADMIN)
  };

  const authorUser: UserType = {
    id: IdVO.create(),
    email: EmailVO.create('author@author.com'),
    password: PasswordVO.create('author'),
    role: RoleVO.create(Role.AUTHOR)
  };

  const userData: UserType = {
    id: IdVO.create(),
    email: EmailVO.create('user@user.com'),
    password: PasswordVO.create('user'),
    role: RoleVO.create(Role.USER)
  };

  if (!await userService.findByEmail(userData.email)) {
    await userService.create(new User(userData));
  }

  if (!await userService.findByEmail(adminUser.email)) {
    await userService.create(new User(adminUser));
  }

  if (!await userService.findByEmail(authorUser.email)) {
    await userService.create(new User(authorUser));
  }
};

export { populateAdmin };

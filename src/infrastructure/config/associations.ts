import { CommentModel } from '../schemas/comment.schema';
import { PostModel } from '../schemas/post.schema';
import { UserModel } from '../schemas/user.shcema';

export const associations = () => {

  UserModel.hasMany(PostModel);
  PostModel.belongsTo(UserModel);

  UserModel.hasMany(CommentModel);
  CommentModel.belongsTo(UserModel);

  PostModel.hasMany(CommentModel, {
    onDelete: 'CASCADE'
  });
  CommentModel.belongsTo(PostModel);
};

import mongoose from 'mongoose';

const PostCacheSchema = new mongoose.Schema({
  id: {
    type: String,
    required: true
  },
  author: {
    type: String,
    required: true
  },
  title: {
    type: String,
    required: true
  },
  content: {
    type: String,
    required: true
  },
  comments: [{
    id: String,
    author: String,
    content: String,
    timestamp: String
  }]
});

// PostCacheSchema.set('toJSON', {
//   transform: (_doc, transformedObject) => {
//     delete transformedObject._id;
//     delete transformedObject.__v;
//   },
// });

export const PostCacheModel = mongoose.model('PostCache', PostCacheSchema);

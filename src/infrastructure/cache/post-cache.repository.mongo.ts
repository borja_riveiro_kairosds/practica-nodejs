/* eslint-disable @typescript-eslint/no-explicit-any */
import Container, { Service } from 'typedi';
import { PostByIdResponse, PostResponse } from '../../application/use-cases/post/post.response';
import { ReadAllPostUsecase } from '../../application/use-cases/post/read-all-post.usecase';
import { ReadByIdPostUsecase } from '../../application/use-cases/post/read-by-id-post.usecase';
import { PostCacheModel } from './post-cache.schema';

@Service()
export class PostCacheRepositoryMongo {
  async updatePostsCache () {
    const postUsecase = Container.get(ReadAllPostUsecase);
    const allPosts = await postUsecase.execute();
    PostCacheModel.deleteMany({}, () => {
      console.log('Clear cache');
    });
    await Promise.all(
      allPosts.map(async (post) => {
        const readByIdPostUsecase = Container.get(ReadByIdPostUsecase);
        const readByIdPost  = await readByIdPostUsecase.execute(post.id);
        await PostCacheModel.create(readByIdPost);
      })
    );
  }

  async readAllPostsCached() {
    const postsCached = await PostCacheModel.find({}).exec();
    const postsCachedResponse = postsCached.map(postCached => {
      const postCachedResponse: PostResponse = {
        id: postCached.id,
        author: postCached.author,
        title: postCached.title,
        content: postCached.content
      };
      return postCachedResponse;
    });
    return postsCachedResponse;
  }

  async readPostByIdCached(postId: string) {
    const postInMongo = await PostCacheModel.findOne({id: postId}).exec();

    if(!postInMongo){
      return null;
    }

    if (postInMongo.comments) {

      const postByIdRepsonse: PostByIdResponse = {
        id: postInMongo.id,
        author: postInMongo.author,
        title: postInMongo.title,
        content: postInMongo.content,
        comments: postInMongo.comments.map((commentInPost: any) => {
          return {
            id: commentInPost.id,
            author: commentInPost.author,
            content: commentInPost.content,
            timestamp: commentInPost.timestamp
          };
        })
      };

      return postByIdRepsonse;
    }

  }
}

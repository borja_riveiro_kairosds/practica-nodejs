import express from 'express';
import Container from 'typedi';
import { PostCacheRepositoryMongo } from './post-cache.repository.mongo';

export const cache = () => {
  return async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction): Promise<express.Response | void> => {

    const { postId } = req.params;
    const postCache = Container.get(PostCacheRepositoryMongo);
    const key = '__cache__' + req.originalUrl;
    console.log(key);


    res.on('finish', async () => {
      await postCache.updatePostsCache();
    });

    // READ ALL
    if (req.method === 'GET' && !postId) {
      const postsFromCache = await postCache.readAllPostsCached();
      if(postsFromCache.length) {
        return res.status(200).json(postsFromCache);
      }
    }

    // READ BY ID
    if (req.method === 'GET' && postId) {
      const postFromCache = await postCache.readPostByIdCached(postId);
      if (postFromCache) {
        return res.status(200).json(postFromCache);
      }
    }

    next();
  };
};

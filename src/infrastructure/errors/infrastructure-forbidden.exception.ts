import { CustomError } from 'ts-custom-error';

export class InfrastructureForbiddenException extends CustomError { }

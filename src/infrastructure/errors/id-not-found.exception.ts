import { InfrastructureNotFoundException } from './infrastructure-not-found.exception';

export class IdNotFoundException extends InfrastructureNotFoundException {
  public constructor(value: string) {
    super(`Id ${value} not found`);
  }
}

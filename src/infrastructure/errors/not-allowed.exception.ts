import { InfrastructureForbiddenException } from './infrastructure-forbidden.exception';

export class NotAllowedException extends InfrastructureForbiddenException {
  public constructor() {
    super('Not allowed');
  }
}

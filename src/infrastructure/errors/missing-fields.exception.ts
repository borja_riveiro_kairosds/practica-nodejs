import { InfrastructureFormatException } from './infrastructure-format.exception';

export class MissingFieldsException extends InfrastructureFormatException {
  public constructor() {
    super('Required fields are missing');
  }
}

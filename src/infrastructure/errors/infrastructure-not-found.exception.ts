import { CustomError } from 'ts-custom-error';

export class InfrastructureNotFoundException extends CustomError { }

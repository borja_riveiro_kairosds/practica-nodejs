import { Router } from 'express';
import { body, oneOf } from 'express-validator';
import passport from 'passport';
import { Role } from '../../domain/model/vos/user/role.vo';
import { createController } from '../controllers/offensive-words/create.controller';
import { deleteController } from '../controllers/offensive-words/delete.controller';
import { readAllController } from '../controllers/offensive-words/read-all.controller';
import { readByIdController } from '../controllers/offensive-words/read-by-id.controller';
import { updateController } from '../controllers/offensive-words/update.controller';
import { hasRole } from '../middlewares/roles';
export const offensiveWordsRouter = Router();

// Create offensive word
offensiveWordsRouter.post('/offensive-word',
  passport.authenticate('jwt', {session: false, failWithError: true }),
  hasRole([Role.ADMIN]),
  body('word').notEmpty(),
  body('level').notEmpty().isNumeric(),
  createController
);

// Read all offensive words
offensiveWordsRouter.get('/offensive-word',
  passport.authenticate('jwt', {session: false, failWithError: true }),
  hasRole([Role.ADMIN]),
  readAllController
);

// Read offensive word by id
offensiveWordsRouter.get('/offensive-word/:id',
  passport.authenticate('jwt', {session: false, failWithError: true }),
  hasRole([Role.ADMIN]),
  readByIdController
);

// Delete offensive word
offensiveWordsRouter.delete('/offensive-word/:id',
  passport.authenticate('jwt', {session: false, failWithError: true }),
  hasRole([Role.ADMIN]),
  deleteController
);

// Update offensive word
offensiveWordsRouter.patch('/offensive-word/:id',
  passport.authenticate('jwt', {session: false, failWithError: true }),
  hasRole([Role.ADMIN]),
  oneOf([
    body('word').notEmpty(),
    body('level').notEmpty().isNumeric()
  ]),
  updateController
);

import { Router } from 'express';
import { body } from 'express-validator';
import passport from 'passport';
import { getRoleController } from '../controllers/user/get-role.controller';
import { loginController } from '../controllers/user/login.controller';
import { registerController } from '../controllers/user/register.controller';

export const userRouter = Router();

// Register
userRouter.post(
  '/register',
  body('email').notEmpty(),
  body('password').notEmpty(),
  registerController
);

// Login
userRouter.post(
  '/login',
  body('email').notEmpty(),
  body('password').notEmpty(),
  loginController
);

// Get Role
userRouter.get('/role/me',
  passport.authenticate('jwt', {session: false, failWithError: true }),
  getRoleController
);

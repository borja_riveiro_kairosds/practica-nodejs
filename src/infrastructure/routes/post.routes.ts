import { Router } from 'express';
import { body } from 'express-validator';
import passport from 'passport';
import { Role } from '../../domain/model/vos/user/role.vo';
import { cache } from '../cache/cache-middleware';
import { createCommentController } from '../controllers/post/comments/create-comment.controller';
import { deleteCommentController } from '../controllers/post/comments/delete-comment.controller';
import { updateCommentController } from '../controllers/post/comments/update-comment.controller';
import { createPostController } from '../controllers/post/create-post.controller';
import { deletePostController } from '../controllers/post/delete-post.controller';
import { readAllPostController } from '../controllers/post/read-all-post.controler';
import { readByIdPostController } from '../controllers/post/read-by-id-post.controller';
import { updatePostController } from '../controllers/post/update-post.controller';
import { hasPermissions, Permission } from '../middlewares/permission';
import { hasRole } from '../middlewares/roles';

export const postRouter = Router();

// Create post
postRouter.post(
  '/posts',
  passport.authenticate('jwt', { session: false, failWithError: true }),
  hasRole([Role.ADMIN, Role.AUTHOR]),
  body('title').notEmpty(),
  body('content').notEmpty(),
  cache(),
  createPostController
);

// Get all post
postRouter.get(
  '/posts',
  // passport.authenticate('jwt', {session: false, failWithError: true }),
  // hasRole([Role.ADMIN, Role.AUTHOR, Role.USER]),
  cache(),
  readAllPostController
);

// Get post by ID
postRouter.get('/posts/:postId', cache(), readByIdPostController);

// Delete post
postRouter.delete(
  '/posts/:postId',
  passport.authenticate('jwt', { session: false, failWithError: true }),
  hasRole([Role.ADMIN, Role.AUTHOR]),
  hasPermissions(Permission.POST),
  cache(),
  deletePostController
);

// Update post
postRouter.put(
  '/posts/:postId',
  passport.authenticate('jwt', { session: false, failWithError: true }),
  hasRole([Role.ADMIN, Role.AUTHOR]),
  hasPermissions(Permission.POST),
  body('title').notEmpty(),
  body('content').notEmpty(),
  cache(),
  updatePostController
);

// Create comment
postRouter.post(
  '/posts/:postId/comments',
  passport.authenticate('jwt', { session: false, failWithError: true }),
  hasRole([Role.ADMIN, Role.AUTHOR, Role.USER]),
  body('content').notEmpty(),
  cache(),
  createCommentController
);

// Delete comment
postRouter.delete(
  '/posts/:postId/comments/:commentId',
  passport.authenticate('jwt', { session: false, failWithError: true }),
  hasRole([Role.ADMIN, Role.AUTHOR, Role.USER]),
  hasPermissions(Permission.COMMENT),
  cache(),
  deleteCommentController
);

// Update comment
postRouter.put(
  '/posts/:postId/comments/:commentId',
  passport.authenticate('jwt', { session: false, failWithError: true }),
  hasRole([Role.ADMIN, Role.AUTHOR, Role.USER]),
  hasPermissions(Permission.COMMENT),
  body('content').notEmpty(),
  updateCommentController
);

import { Router } from 'express';
import { statusController } from '../controllers/status/status.controller';

export const statusRouter = Router();

// Status
statusRouter.get(
  '/status',
  statusController
);

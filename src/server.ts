import app from './app';
import { associations } from './infrastructure/config/associations';
import { connectToDataBase } from './infrastructure/config/mongo';
import './infrastructure/config/postgresql';

connectToDataBase();

associations();

const PORT = process.env.API_PORT ?? 3000;

app.listen(PORT, () => {
  console.log(`Server up on port ${PORT}`);
});

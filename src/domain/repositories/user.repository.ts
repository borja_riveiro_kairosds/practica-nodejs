import { User } from '../model/entities/user.entity';
import { EmailVO } from '../model/vos/user/email.vo';

export interface UserRepository {
  create(user: User): Promise<void>;
  findByEmail(email: EmailVO): Promise<User | null>;
}

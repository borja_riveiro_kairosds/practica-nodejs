import { Comment } from '../model/entities/comment.entity';
import { Post } from '../model/entities/post.entity';
import { IdVO } from '../model/vos/id.vo';

export interface PostRepository {
  create(post: Post): void;
  findAll(): Promise<Post[]>;
  findById(id: IdVO): Promise<Post | null>;
  delete(id: IdVO): Promise<Post | null>;
  update(post: Post): Promise<Post | null>;
  createComment(postId: IdVO, comment: Comment): Promise<void>;
  deleteComment(postId: IdVO, commentId: IdVO ): Promise<Comment>
  updateComment(postId: IdVO, comment: Comment): Promise<Comment>
  findCommentById(commentId: IdVO): Promise<Comment>
}

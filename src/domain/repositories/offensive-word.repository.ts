import { OffensiveWord } from '../model/entities/offensive-word.entity';
import { IdVO } from '../model/vos/id.vo';

export interface OffensiveWordRepository {
  save (offensiveWord: OffensiveWord): void
  findAll(): Promise<OffensiveWord[]>
  findById(id: IdVO): Promise<OffensiveWord | null>
  delete(id: IdVO): Promise<OffensiveWord | null>
  update(offensiveWord: OffensiveWord): Promise<OffensiveWord | null>
}

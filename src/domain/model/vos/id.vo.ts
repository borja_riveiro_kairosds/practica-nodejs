import { v4 as uuid, validate } from 'uuid';
import { VOFormatException } from '../../errors/vo-format.exception';

export class IdVO {

  public get value() : string {
    return this.id;
  }

  private constructor (private id: string) {}

  static create(): IdVO {
    return new IdVO(uuid());
  }

  static createWithUUID(uuid: string): IdVO {
    if (!validate(uuid)) {
      throw new VOFormatException('uuid', uuid);
    }
    return new IdVO(uuid);
  }
}

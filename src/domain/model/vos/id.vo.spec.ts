import { v4 as uuid, validate } from 'uuid';
import { VOFormatException } from '../../errors/vo-format.exception';
import { IdVO } from './id.vo';

describe('Id ValueObject', () => {
  it('Should create', () => {
    const id = IdVO.create();
    expect(validate(id.value)).toBeTruthy();
  });

  it('Shoul create id with uuid', () => {
    const id = IdVO.createWithUUID(uuid());
    expect(id.value).toBeTruthy();
  });

  it('Shoul throw error when id in not valid', () => {
    expect(() => IdVO.createWithUUID('no-valid-uuid')).toThrow(VOFormatException);
  });
});

import { Role, RoleVO } from './role.vo';

describe ('Role Value Objext', () => {
  it('Should create with ADMIN role', () => {
    const role = RoleVO.create(Role.ADMIN);
    expect(role.value).toEqual(Role.ADMIN);
  });

  it('Should create with USER role', () => {
    const role = RoleVO.create(Role.USER);
    expect(role.value).toEqual(Role.USER);
  });
});

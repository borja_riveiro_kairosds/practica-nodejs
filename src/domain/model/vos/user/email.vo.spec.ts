import { VOFormatException } from '../../../errors/vo-format.exception';
import { EmailVO } from './email.vo';

describe ('Email Value Object', () => {
  it('Should create', () => {
    const email = EmailVO.create('test@test.es');
    expect(email.value).toEqual('test@test.es');
  });

  it('should throw error if email is invalid', () => {
    const invalidEmail = 'test';
    expect(() => EmailVO.create(invalidEmail)).toThrow(VOFormatException);
  });
});

import { PasswordVO } from './password.vo';

describe ('Password Value Object', () => {
  it('Should create', () => {
    const password = PasswordVO.create('passwordTest');
    expect(password.value).toEqual('passwordTest');
  });
});

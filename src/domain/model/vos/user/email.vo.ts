import { VOFormatException } from '../../../errors/vo-format.exception';

const EMAIL_REGEX =
/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export class EmailVO {

  public get value() : string {
    return this.email;
  }

  private constructor (private email: string) {}

  static create(email: string): EmailVO {

    if (!EMAIL_REGEX.test(email)) {
      throw new VOFormatException('email', email);
    }

    return new EmailVO(email);
  }

}

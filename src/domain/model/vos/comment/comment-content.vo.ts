import { CheckOffensiveWordsException } from '../../../errors/check-offensive-words.exception';
import { VOFormatWithLimitsException } from '../../../errors/vo-format-with-limits.exception';
import { VORequiredException } from '../../../errors/vo-required.exception';
import { checkOffensiveWords } from '../../../services/check-offensive-word.service';
import { OffensiveWord } from '../../entities/offensive-word.entity';

export class CommentContentVO {

  private static readonly MIN_LENGTH = 10;
  private static readonly MAX_LENGTH = 200;

  public get value() : string {
    return this.commentContent;
  }

  private constructor( private commentContent: string) {}

  static create(commentContent: string , offensiveWords: OffensiveWord[] = []) {

    if (!commentContent) {
      throw new VORequiredException('Comment content');
    }

    if (commentContent.length < this.MIN_LENGTH || commentContent.length > this.MAX_LENGTH) {
      throw new VOFormatWithLimitsException(commentContent, 'comment content', this.MIN_LENGTH, this.MAX_LENGTH );
    }

    const offensiveWordsFound = checkOffensiveWords(commentContent, offensiveWords);
    const offensiveWordsList = offensiveWordsFound.map((offensiveWord) =>  offensiveWord.word);

    if (offensiveWordsFound.length > 0) {
      throw new CheckOffensiveWordsException(offensiveWordsList.join(', '));
    }

    return new CommentContentVO(commentContent);
  }
}

import { VOFormatWithLimitsException } from '../../../errors/vo-format-with-limits.exception';
import { VORequiredException } from '../../../errors/vo-required.exception';
import { CommentContentVO } from './comment-content.vo';

describe('Content ValueObject', () => {
  it('Should create', () => {
    const newCommentContent = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean m';
    const commentContent = CommentContentVO.create(newCommentContent);
    expect(commentContent.value).toEqual(newCommentContent);
  });

  it('Should throw error when content is empty',() => {
    const newContent = '';
    expect(() => CommentContentVO.create(newContent)).toThrow(VORequiredException);
  });

  it('Should throw error when then length is greater',() => {
    const newCommentContent = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qua';
    expect(() => CommentContentVO.create(newCommentContent)).toThrow(VOFormatWithLimitsException);
  });

  it('Should throw error when then length is smaller',() => {
    const newCommentContent = 'Lorem ips';
    expect(() => CommentContentVO.create(newCommentContent)).toThrow(VOFormatWithLimitsException);
  });
});

export class TimestampVO {


  public get value() : string {
    return this.timestamp;
  }

  private constructor(private timestamp: string) {}

  static create() {
    return new TimestampVO(Date.now().toString());
  }

  static createWithTimestamp(timestamp: string): TimestampVO {
    return new TimestampVO(timestamp);
  }


}

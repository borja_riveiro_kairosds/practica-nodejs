import { VOFormatWithLimitsException } from '../../../errors/vo-format-with-limits.exception';
import { VORequiredException } from '../../../errors/vo-required.exception';

export class ContentVO {

  private static readonly MIN_LENGTH = 50;
  private static readonly MAX_LENGTH = 300;

  public get value() : string {
    return this.content;
  }

  private constructor( private content: string) {}

  static create(content: string) {

    if (!content) {
      throw new VORequiredException('Content');
    }

    if (content.length < this.MIN_LENGTH || content.length > this.MAX_LENGTH) {
      throw new VOFormatWithLimitsException(content, 'content', this.MIN_LENGTH, this.MAX_LENGTH );
    }

    return new ContentVO(content);
  }

}

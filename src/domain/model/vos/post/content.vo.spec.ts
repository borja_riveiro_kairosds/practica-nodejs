import { VOFormatWithLimitsException } from '../../../errors/vo-format-with-limits.exception';
import { VORequiredException } from '../../../errors/vo-required.exception';
import { ContentVO } from './content.vo';

describe('Content ValueObject', () => {
  it('Should create', () => {
    const newContent = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean m';
    const content = ContentVO.create(newContent);
    expect(content.value).toEqual(newContent);
  });

  it('Should throw error when content is empty',() => {
    const newContent = '';
    expect(() => ContentVO.create(newContent)).toThrow(VORequiredException);
  });

  it('Should throw error when then length is greater',() => {
    const newContent = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec p';
    expect(() => ContentVO.create(newContent)).toThrow(VOFormatWithLimitsException);
  });

  it('Should throw error when then length is smaller',() => {
    const newContent = 'Lorem ipsum dolor sit amet, consectetuer adipisci';
    expect(() => ContentVO.create(newContent)).toThrow(VOFormatWithLimitsException);
  });
});

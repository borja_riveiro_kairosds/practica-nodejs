import { VOFormatWithLimitsException } from '../../../errors/vo-format-with-limits.exception';
import { VORequiredException } from '../../../errors/vo-required.exception';
import { TitleVO } from './title.vo';

describe('title ValueObject', () => {
  it('Should create', () => {
    const newTitle = 'API REST con NodeJs';
    const title = TitleVO.create(newTitle);
    expect(title.value).toEqual(newTitle);
  });

  it('Should throw error when title is empty',() => {
    const newTitle = '';
    expect(() => TitleVO.create(newTitle)).toThrow(VORequiredException);
  });

  it('Should throw error when then length is greater',() => {
    const newTitle = 'API REST con NodeJS con el framework express';
    expect(() => TitleVO.create(newTitle)).toThrow(VOFormatWithLimitsException);
  });

  it('Should throw error when then length is smaller',() => {
    const newTitle = 'Node';
    expect(() => TitleVO.create(newTitle)).toThrow(VOFormatWithLimitsException);
  });
});

import { VOFormatWithLimitsException } from '../../../errors/vo-format-with-limits.exception';
import { VORequiredException } from '../../../errors/vo-required.exception';

export class TitleVO {

  private static readonly MIN_LENGTH = 5;
  private static readonly MAX_LENGTH = 30;

  public get value() : string {
    return this.title;
  }

  private constructor( private title: string) {}

  static create(title: string) {

    if (!title) {
      throw new VORequiredException('Title');
    }

    if (title.length < this.MIN_LENGTH || title.length > this.MAX_LENGTH) {
      throw new VOFormatWithLimitsException(title, 'title', this.MIN_LENGTH, this.MAX_LENGTH );
    }

    return new TitleVO(title);
  }

}

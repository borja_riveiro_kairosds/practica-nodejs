import { VOFormatWithLimitsException } from '../../../errors/vo-format-with-limits.exception';
import { LevelVO } from './level.vo';

describe ('Level ValueObject', () => {
  it('Should create', () => {
    const minLevel = 1;
    const level = LevelVO.create(minLevel);
    expect(level.value).toEqual(minLevel);
  });

  it('should throw error if level is lower', () => {
    const invalidLevel = 0;
    expect(() => LevelVO.create(invalidLevel)).toThrow(VOFormatWithLimitsException);
  });

  it('should throw error if level is greater', () => {
    const invalidLevel = 6;
    expect(() => LevelVO.create(invalidLevel)).toThrow(VOFormatWithLimitsException);
  });
});

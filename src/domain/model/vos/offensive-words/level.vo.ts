import { VOFormatWithLimitsException } from '../../../errors/vo-format-with-limits.exception';

export class LevelVO {

  private static readonly MIN_LENGTH = 1;
  private static readonly MAX_LENGTH = 5;

  public get value() : number {
    return this.level;
  }

  private constructor (private level: number) {}

  static create(level: number): LevelVO {

    if (level < this.MIN_LENGTH || level > this.MAX_LENGTH) {
      throw new VOFormatWithLimitsException(level, 'level', this.MIN_LENGTH, this.MAX_LENGTH );
    }

    return new LevelVO(level);
  }

}

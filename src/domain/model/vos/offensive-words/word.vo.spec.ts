import { WordVO } from './word.vo';

describe('Word ValueObject', () => {
  it('Should create', () => {
    const newWorld = 'word';
    const word = WordVO.create(newWorld);
    expect(word.value).toEqual(newWorld);
  });
});

import { IdVO } from '../vos/id.vo';
import { LevelVO } from '../vos/offensive-words/level.vo';
import { WordVO } from '../vos/offensive-words/word.vo';

export type OffensiveWordType = {
  id: IdVO,
  word: WordVO,
  level: LevelVO
}

export class OffensiveWord {

  constructor (private offensiveWord: OffensiveWordType) {}


  public get id() : string {
    return this.offensiveWord.id.value;
  }

  public get word() : string {
    return this.offensiveWord.word.value;
  }

  public get level() : number {
    return this.offensiveWord.level.value;
  }
}

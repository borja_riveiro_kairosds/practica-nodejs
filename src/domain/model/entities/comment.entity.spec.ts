import { CommentContentVO } from '../vos/comment/comment-content.vo';
import { TimestampVO } from '../vos/comment/timestamp.vo';
import { IdVO } from '../vos/id.vo';
import { EmailVO } from '../vos/user/email.vo';
import { PasswordVO } from '../vos/user/password.vo';
import { Role, RoleVO } from '../vos/user/role.vo';
import { Comment, CommentType } from './comment.entity';
import { User } from './user.entity';
describe('Comment Entity', () => {
  it('Should create', () => {
    const commentType: CommentType = {
      id: IdVO.create(),
      author:  new User({
        id: IdVO.create(),
        email: EmailVO.create('test@test.es'),
        password: PasswordVO.create('passwordTest'),
        role: RoleVO.create(Role.ADMIN)
      }),
      content: CommentContentVO.create('Este es un comentario para el test'),
      timestamp: TimestampVO.create()
    };

    const comment = new Comment(commentType);

    expect(comment.id.value).toEqual(commentType.id.value);
    expect(comment.author.id.value).toEqual(commentType.author.id.value);
    expect(comment.author.email.value).toEqual(commentType.author.email.value);
    expect(comment.author.password.value).toEqual(commentType.author.password.value);
    expect(comment.author.role.value).toEqual(commentType.author.role.value);
    expect(comment.content.value).toEqual(commentType.content.value);
    expect(comment.timestamp.value).toEqual(commentType.timestamp.value);
  });

});

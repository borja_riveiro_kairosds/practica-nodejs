import { IdVO } from '../vos/id.vo';
import { EmailVO } from '../vos/user/email.vo';
import { PasswordVO } from '../vos/user/password.vo';
import { RoleVO } from '../vos/user/role.vo';

export type UserType = {
  id: IdVO,
  email: EmailVO,
  password: PasswordVO,
  role: RoleVO
}

export class User {

  constructor (private user: UserType) {}

  public get id() : IdVO {
    return this.user.id;
  }

  public get email() : EmailVO {
    return this.user.email;
  }

  public get password() : PasswordVO {
    return this.user.password;
  }

  public get role() : RoleVO {
    return this.user.role;
  }
}

import { IdVO } from '../vos/id.vo';
import { ContentVO } from '../vos/post/content.vo';
import { TitleVO } from '../vos/post/title.vo';
import { EmailVO } from '../vos/user/email.vo';
import { PasswordVO } from '../vos/user/password.vo';
import { Role, RoleVO } from '../vos/user/role.vo';
import { Post, PostType } from './post.entity';
import { User } from './user.entity';
describe('Post Entity', () => {
  it('Should create', () => {
    const postType: PostType = {
      id: IdVO.create(),
      author:  new User({
        id: IdVO.create(),
        email: EmailVO.create('test@test.es'),
        password: PasswordVO.create('passwordTest'),
        role: RoleVO.create(Role.ADMIN)
      }),
      title: TitleVO.create('Title test'),
      content: ContentVO.create('Contenido de un post para el test unitario con un minimo de 50 caracteres'),
      comments: []
    };

    const post = new Post(postType);

    expect(post.id.value).toEqual(postType.id.value);
    expect(post.author.id.value).toEqual(postType.author.id.value);
    expect(post.author.email.value).toEqual(postType.author.email.value);
    expect(post.author.password.value).toEqual(postType.author.password.value);
    expect(post.author.role.value).toEqual(postType.author.role.value);
    expect(post.title.value).toEqual(postType.title.value);
    expect(post.content.value).toEqual(postType.content.value);
  });

});

import { CommentContentVO } from '../vos/comment/comment-content.vo';
import { TimestampVO } from '../vos/comment/timestamp.vo';
import { IdVO } from '../vos/id.vo';
import { User } from './user.entity';

export type CommentType = {
  id: IdVO,
  author: User,
  content: CommentContentVO,
  timestamp: TimestampVO,
}

export class Comment {

  constructor (private comment: CommentType) {}

  public get id() : IdVO {
    return this.comment.id;
  }

  public get author() : User {
    return this.comment.author;
  }

  public get content() : CommentContentVO {
    return this.comment.content;
  }

  public get timestamp() : TimestampVO {
    return this.comment.timestamp;
  }
}

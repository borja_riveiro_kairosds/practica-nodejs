import { IdVO } from '../vos/id.vo';
import { LevelVO } from '../vos/offensive-words/level.vo';
import { WordVO } from '../vos/offensive-words/word.vo';
import { OffensiveWord, OffensiveWordType } from './offensive-word.entity';
describe('Offensive Word Entity', () => {
  it('Should create', () => {
    const offensiveWordData: OffensiveWordType = {
      id: IdVO.create(),
      word: WordVO.create('capullo'),
      level: LevelVO.create(4)
    };

    const offensiveWord = new OffensiveWord(offensiveWordData);

    expect(offensiveWord.id).toEqual(offensiveWordData.id.value);
    expect(offensiveWord.word).toEqual(offensiveWordData.word.value);
    expect(offensiveWord.level).toEqual(offensiveWordData.level.value);
  });

});

import { IdVO } from '../vos/id.vo';
import { ContentVO } from '../vos/post/content.vo';
import { TitleVO } from '../vos/post/title.vo';
import { Comment } from './comment.entity';
import { User } from './user.entity';

export type PostType = {
  id: IdVO,
  author: User,
  title: TitleVO,
  content: ContentVO,
  comments: Comment[]
}

export class Post {

  constructor (private post: PostType) {}


  public get id() : IdVO {
    return this.post.id;
  }

  public get author() : User {
    return this.post.author;
  }

  public get title() : TitleVO {
    return this.post.title;
  }

  public get content() : ContentVO {
    return this.post.content;
  }

  public get comments() : Comment[] {
    return this.post.comments;
  }

}

import { IdVO } from '../vos/id.vo';
import { EmailVO } from '../vos/user/email.vo';
import { PasswordVO } from '../vos/user/password.vo';
import { Role, RoleVO } from './../vos/user/role.vo';
import { User, UserType } from './user.entity';

describe('User Entity', () => {

  it('should create', () => {
    const userData: UserType = {
      id: IdVO.create(),
      email: EmailVO.create('test@test.es'),
      password: PasswordVO.create('passwordTest'),
      role: RoleVO.create(Role.ADMIN)
    };

    const user = new User(userData);

    expect(user.id.value).toEqual(userData.id.value);
    expect(user.email.value).toEqual(userData.email.value);
    expect(user.password.value).toEqual(userData.password.value);
    expect(user.role.value).toEqual(userData.role.value);
  });

});

import { DomainFormatException } from './domain-format.exception';

export class VOFormatException extends DomainFormatException {
  public constructor(constructorName: string, value: string | number) {
    super(`Value ${value} has an invalid ${constructorName} format`);
  }
}

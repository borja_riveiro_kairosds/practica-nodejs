import { DomainFormatException } from './domain-format.exception';

export class CheckOffensiveWordsException extends DomainFormatException {
  public constructor(value: string) {
    super(`The commentary includes the following offensive words: ${value}`);
  }
}

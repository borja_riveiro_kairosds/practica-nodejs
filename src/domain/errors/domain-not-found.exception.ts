import { CustomError } from 'ts-custom-error';

export class DomainNotFoundException extends CustomError { }

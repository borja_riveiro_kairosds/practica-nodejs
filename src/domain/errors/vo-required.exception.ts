import { DomainFormatException } from './domain-format.exception';

export class VORequiredException extends DomainFormatException {
  public constructor(constructorName: string) {
    super(`${constructorName} is required.`);
  }
}

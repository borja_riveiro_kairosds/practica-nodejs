import { DomainFormatException } from './domain-format.exception';

export class VOFormatWithLimitsException extends DomainFormatException {
  public constructor(value: string | number, constructorName: string, minLength: number, maxLength: number) {
    super(`Value ${value} is an invalid ${constructorName}. Must be between ${minLength} and ${maxLength}`);
  }
}

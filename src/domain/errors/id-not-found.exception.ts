import { DomainNotFoundException } from './domain-not-found.exception';

export class IdNotFoundException extends DomainNotFoundException {
  public constructor(value: string) {
    super(`Id ${value} not found`);
  }
}

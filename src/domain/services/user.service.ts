import { compare, hash } from 'bcrypt';
import { Inject, Service } from 'typedi';
import { User, UserType } from '../model/entities/user.entity';
import { EmailVO } from '../model/vos/user/email.vo';
import { PasswordVO } from '../model/vos/user/password.vo';
import { UserRepository } from '../repositories/user.repository';


@Service()
export class UserService {
  constructor(@Inject('UserRepository') private userRepository: UserRepository) {}

  public async create(user: User): Promise<void> {
    const hashedPassword = await hash(user.password.value, 10);
    const encryptPassword = PasswordVO.create(hashedPassword);
    const newUser: UserType = {
      id: user.id,
      email: user.email,
      password: encryptPassword,
      role: user.role
    };
    await this.userRepository.create(new User(newUser));
  }

  public async findByEmail(email: EmailVO): Promise<User | null> {
    return await this.userRepository.findByEmail(email);
  }

  public async isValidPassword(password: PasswordVO, user: User): Promise<boolean> {
    return compare(password.value, user.password.value);
  }
}

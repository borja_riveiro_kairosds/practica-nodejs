import { Inject, Service } from 'typedi';
import { IdNotFoundException } from '../errors/id-not-found.exception';
import { OffensiveWord, OffensiveWordType } from '../model/entities/offensive-word.entity';
import { IdVO } from '../model/vos/id.vo';
import { OffensiveWordRepository } from '../repositories/offensive-word.repository';

@Service()
export class OffensiveWordService {

  constructor(@Inject('OffensiveWordRepository') private offensiveWordRepository: OffensiveWordRepository) {}

  public persist(offensiveWord: OffensiveWordType): void {
    const offensiveWordEntity = new OffensiveWord(offensiveWord);
    this.offensiveWordRepository.save(offensiveWordEntity);
  }

  public async findAll(): Promise<OffensiveWord[]> {
    return await this.offensiveWordRepository.findAll();
  }

  public async findById(id: IdVO): Promise<OffensiveWord | null> {
    const offensiveWord =  await this.offensiveWordRepository.findById(id);
    if (!offensiveWord) {
      throw new IdNotFoundException(id.value);
    }
    return offensiveWord;
  }

  public async delete(id: IdVO): Promise<OffensiveWord | null> {
    await this.checkIdExits(id);

    return await this.offensiveWordRepository.delete(id);
  }

  public async update(offensiveWord: OffensiveWordType): Promise<OffensiveWord | null> {
    const offensiveWordEntity = new OffensiveWord(offensiveWord);
    await this.checkIdExits(offensiveWord.id);
    return await this.offensiveWordRepository.update(offensiveWordEntity);
  }

  private async checkIdExits(id: IdVO): Promise<void> {
    const offensiveWord = await this.findById(id);
    if (!offensiveWord) {
      throw new IdNotFoundException(id.value);
    }
  }

}

import { Inject, Service } from 'typedi';
import { Comment } from '../model/entities/comment.entity';
import { Post } from '../model/entities/post.entity';
import { IdVO } from '../model/vos/id.vo';
import { PostRepository } from '../repositories/post.repository';


@Service()
export class PostService {
  constructor(@Inject('PostRepository') private postRepository: PostRepository) {}

  async create(post: Post): Promise<void> {
    return this.postRepository.create(post);
  }

  async findAll(): Promise<Post[]> {
    return this.postRepository.findAll();
  }

  async findById(postId: IdVO): Promise<Post | null> {
    return this.postRepository.findById(postId);
  }

  async delete(postId: IdVO): Promise<Post | null> {
    return this.postRepository.delete(postId);
  }

  async update(post: Post): Promise<Post | null> {
    return this.postRepository.update(post);
  }

  async createComment(postId: IdVO, comment: Comment): Promise<void> {
    return this.postRepository.createComment(postId, comment);
  }

  async deleteComment(postId: IdVO, commentId: IdVO): Promise<Comment> {
    return this.postRepository.deleteComment(postId, commentId);
  }

  async updateComment(postId: IdVO, comment: Comment): Promise<Comment> {
    return this.postRepository.updateComment(postId, comment);
  }

  async findCommentById(commentId: IdVO): Promise<Comment> {
    return this.postRepository.findCommentById(commentId);
  }

}

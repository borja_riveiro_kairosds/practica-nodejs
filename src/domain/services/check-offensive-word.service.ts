import { OffensiveWord } from './../model/entities/offensive-word.entity';

export const checkOffensiveWords = (commentContent: string, offensiveWords: OffensiveWord[]): OffensiveWord[] => {

  const contentWords = commentContent.toLocaleLowerCase().split(' ');
  let offensiveWordsFound: OffensiveWord[] = [];

  contentWords.forEach(contentWord => {
    const findOffensiveWordInContent = offensiveWords.find(offensiveWord => offensiveWord.word.toLocaleLowerCase() === contentWord);
    if (findOffensiveWordInContent) {
      offensiveWordsFound = [...offensiveWordsFound, findOffensiveWordInContent];
    }
  });
  return offensiveWordsFound;
};

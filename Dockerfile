FROM node:16.15.1-alpine AS build
RUN mkdir /build
WORKDIR /build
COPY . .
RUN npm install
RUN npm run build
RUN npm prune --production

FROM node:16.15.1-alpine
WORKDIR /app
COPY --from=build /build/package.json ./package.json
COPY --from=build /build/package-lock.json ./package-lock.json
COPY --from=build /build/dist ./dist
COPY --from=build /build/node_modules ./node_modules
COPY --from=build /build/open-api.yaml ./open-api.yaml
EXPOSE 3000
CMD ["npm", "start"]
